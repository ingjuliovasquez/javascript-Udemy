/*             VARIABLES               */

const marca = document.querySelector('#marca');
const year = document.querySelector('#year');
const minimo = document.querySelector('#minimo');
const maximo = document.querySelector('#maximo');
const puertas = document.querySelector('#puertas');
const transmision = document.querySelector('#transmision');
const color = document.querySelector('#color');

 /*             CONTENEDOR PARA LOS RESULTADOS       */
const resultado = document.querySelector('#resultado') //selecciona el id de resultado 


const max = new Date().getFullYear(); //seleciona el año actual 
const min = max - 10; //resta 10 años al año actual

//Gerenerar un objeto con la busqueda
const datosBusqueda = {
    marca: '',
    year: '',
    minimo: '',
    maximo: '',
    puertas: '',
    transmision: '',
    color: '',

}


/*              EVENTOS                  */
document.addEventListener('DOMContentLoaded', () => { //prepara todo el HTML 
    mostrarAutos(autos); //muestra los automoviles al cargar

    llenarSelect();
})

/*  EVENT LISTENER PARA LOS SELECT DE BUSQUEDA    */
marca.addEventListener('change', e => { //responde al evento de cambio  
    datosBusqueda.marca = e.target.value;

    filtrarAuto();
});
year.addEventListener('change', e => { //responde al evento de cambio  
    datosBusqueda.year = parseInt( e.target.value ); //convierte un string en un numero
    filtrarAuto();  
});
minimo.addEventListener('change', e => { //responde al evento de cambio  
    datosBusqueda.minimo = e.target.value;

    filtrarAuto();  
});
maximo.addEventListener('change', e => { //responde al evento de cambio  
    datosBusqueda.maximo = e.target.value;

    filtrarAuto();
});
puertas.addEventListener('change', e => { //responde al evento de cambio  
    datosBusqueda.puertas = parseInt(e.target.value);

    filtrarAuto();

});
transmision.addEventListener('change', e => { //responde al evento de cambio  
    datosBusqueda.transmision = e.target.value;

    filtrarAuto();

});
color.addEventListener('change', e => { //responde al evento de cambio  
    datosBusqueda.color = e.target.value;

    filtrarAuto();

});


/*              FUNCIONES                */
 function mostrarAutos(autos) {
    LimpiarHTML(); //elimina el html previo
    autos.forEach( auto => { //crea un nuevo arreglo llamado autos este es el principal 

        const { marca, modelo, year, puertas, transmision, precio, color } = auto;
        const autoHTML = document.createElement('P');

        autoHTML.textContent = `
            ${marca} ${modelo} - ${year} - ${puertas} Puertas - Transmisión: ${transmision} - Precio: ${precio} - Color: ${color} 
        `;

        //insertar en el HTML
        resultado.appendChild(autoHTML)
    })
 }

 //Limpiar html 
 function LimpiarHTML() {
    while(resultado.firstChild) {
        resultado.removeChild(resultado.firstChild);
    }
 }

 //Gerenerar los años del Select 
 function llenarSelect() {
    
    for( let i = max; i > min; i--) {
        const opcion = document.createElement('option');
        opcion.value = i;
        opcion.textContent = i;
        year.appendChild(opcion); //Agrega las opciones de año al select
    }
 }

 //funcion  que filtra en base a la busqueda 
 function filtrarAuto() {
    const resultado = autos.filter( filtrarMarca ).filter( filtrarYear ).filter( filtrarMinimo ).filter( filtrarMaximo ).filter( filtrarPuertas ).filter( filtrarTransmision ).filter( filtrarColor );  //pasa a una funcion como parametro

    if( resultado.length) { //se mostraran los resulatdos si hay 
        mostrarAutos(resultado);
    } else {
        noResultado(); //si no hay ningun resultadm aparecera un mensaje 
    }
 } 

 function noResultado () {

    LimpiarHTML(); 

    const noResultado = document.createElement('DIV');
    noResultado.classList.add('alerta', 'error'); //agregagamos dos nuevas clases 
    noResultado.textContent = 'No hay Resultado, intenta con otros terminos de busqueda'; //mensaje de la alerta
    resultado.appendChild(noResultado); //aparecera la alerta al final del documento
 }
 function filtrarMarca(auto) { //iteramos en cada uno de los automiviles 
    const { marca } = datosBusqueda;//Selecciona los autos de la maraca especifica
    if( marca ) {
        return auto.marca === marca; // compara si hay un valor con esa marca  
    }
    return auto;
 }
 function filtrarYear(auto) {
    const { year } = datosBusqueda;
    if( year ) {
        return auto.year === year; // comparador estricto 
    }
    return auto;
}
function filtrarMinimo(auto) {
    const { minimo } = datosBusqueda;
    if( minimo ) {
        return auto.precio >= minimo; // se asegura que el precio sea mayor o igual al precio minimo que estamos seleccionanado 
    }
    return auto;
}
function filtrarMaximo(auto) {
    const { maximo } = datosBusqueda;
    if( maximo ) {
        return auto.precio <= maximo; // se asegura que el precio sea menor o igual al precio minimo que estamos seleccionanado esto nos ayudara a tener un rango de precio
    }
    return auto;
}
function filtrarPuertas(auto) {
    const { puertas } = datosBusqueda;
    if( puertas ) {
        return auto.puertas === puertas; 
    }
    return auto;
}
function filtrarTransmision(auto) {
    const { transmision } = datosBusqueda;
    if( transmision ) {
        return auto.transmision === transmision; 
    }
    return auto;
}
function filtrarColor(auto) {
    const { color } = datosBusqueda;
    if( color ) {
        return auto.color === color; 
    }
    return auto;
}
