const producto = {
    nombre: "Monitor 20 pulgadas",
    precio: 300,
    disponible: true,
}

const medidas = {
    peso: "1kg",
    medidas: '1m'
}

console.log(producto);
console.log(medidas);

//une la variable producto con la de medidas
const resultado = Object.assign(producto, medidas);


//Spreand Operator o Rest Operator de igual forma une las variables
//une las variables
//Esta es la mas utilizada
const resultado2 = {...producto, ...medidas} // los 3 puntos significan que copian la variable

console.log(resultado);
console.log(resultado2);