//Destructuring de objetos anidados
const producto = {
    nombre: "Monitor de 20 pulgadas",
    precio: 300,
    disponible: true,
    informacion: {
        medidas: {
            peso: '1Kg',
            medidas: '1m'
        },
        fabricacion: {
            pais: 'China'
        }
    }
} 

const { nombre, informacion, informacion: {fabricacion, fabricacion: { pais}}} = producto; //se utiliza dos puntos y corchetes para acceder y dividir cada uno de las propiedades

console.log(nombre);
console.log(informacion); //se utiliza informacion, para indicar que queremos acceder al contenido de la propiedad de informacion
console.log(fabricacion); // se utiliza { fabricacion, } para acceder al contenido de la propiedad de fabricacion
console.log(pais);// se utiliza fabricacion: {pais} para acceder de forma mas especifica es decir al pais

// coma es para acceder al objeto en genneral 
// dos puntos y comillas se uliza para acceder de manera mas especfica a algun de los componentes del contenido del objeto