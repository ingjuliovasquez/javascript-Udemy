const producto = {
    nombre: "Monitor de 20 pulgadas",
    precio: 300,
    disponible: true,
}

/*const nombre = producto.nombre;
console.log(nombre);*/

//Destructuring o destructurando  permite desempacar valores de arreglos o propiedades de objetos en distintas variables.
const { nombre, precio, disponible} = producto;
console.log(nombre);
console.log(precio);
console.log(disponible);