const producto = {
    nombre: "Monitor 20 pulgadas",
    precio: 300,
    disponible: true
}

console.log(Object.keys( producto)); //nos permite saber la parte izquierda del objeto como nombre: precio: disponible: 

console.log(Object.values( producto)); //nos permite saber la parte izquierda del objeto como "Monitor 20 pulgadas", 300, true

console.log(Object.entries( producto)); //nos hace saber todo lo que incluye un producto pero en pares