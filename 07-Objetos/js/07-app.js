const producto = {
    nombre: "Monitor de 20 pulgadas",
    precio: 300,
    disponible: true,
}


//las varieandtes no se pueden reasignar pero las propiedades individualente si se pueden reasignar 
producto.disponible = false;

delete producto.precio; //incluso si se pueden eliminar las propiedades de un objeto

console.log(producto);