const producto = {
    nombre: "Monitor de 20 pulgadas",
    precio: 300,
    disponible: true,
}
console.log(producto);


//permite acceder a cada uno de los valores del objeto

//ambas opciones son validas
console.log(producto.nombre); //esta es la mas comun
console.log(producto['nombre']);


console.log(producto.precio);
console.log(producto.disponible);



