"use strict" //hace que en JavaScript no haya errores

const producto = {
    nombre: "Monitor de 20 pulgadas",
    precio: 300,
    disponible: true,
}

Object.seal(producto); //permite modificar las propiedades ya existentes pero no permite agregar nuevas o borrar alguna
/*producto.disponible = false;
producto.imagen = "imagen.jpg";
delete producto.precio; */

console.log(producto);

console.log(Object.isSealed(producto)); //nos permite saber si un objeto esta sellado es esta caso es true