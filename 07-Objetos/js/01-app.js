
const nombre = "Monito de 20 pulgadas";
const precio = 300;
const disponible = true;

//Un objecto agrupa todo en una sola variable

//Objeto literal 
const producto = {
    nombre: "Monitor de 20 pulgadas",
    precio: 300,
    disponible: true,
}

console.log(producto);