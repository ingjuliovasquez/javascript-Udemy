//funciones en objetos y acceder a sus valores
const producto = {
    nombre: "Monitor 20 pulgadas",
    precio: 300,
    disponible: true,
    mostrarINfo: function() {
        console.log(`El Producto: ${this.nombre} tiene un precio de: ${this.precio}`);  //this busca la propiedad nombre y precio dentro del mismo objeto

        //El resultado de la consola es: "El producto: Monitor 20 pulgadas tiene un precio de: 300"
    }
}
const producto2 = {
    nombre: "Tablet",
    precio: 3000,
    disponible: true,
    mostrarINfo: function() {
        console.log(`El Producto: ${this.nombre} tiene un precio de: ${this.precio}`);
    }
}
producto.mostrarINfo();