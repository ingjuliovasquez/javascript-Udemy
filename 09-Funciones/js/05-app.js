
function sumar(a, b) {  //a y b son parametros variables
    console.log( a + b);
}
// se puede sumar cualquier valor
sumar(2, 3); //2 y 3 son argumentos
sumar(200, 184);
sumar(124, 321);

function saludar(nombre, apellido) { //son variables porque pueden tomar cualquier nombre de cualquier personalbar, es util en redes sociales 
    console.log(`Hola ${nombre} ${apellido}`);
}

saludar('Maadai', 'Munguia');
saludar();