//Ambas dan el mismo resultado
const aprendiendo = function() {
    console.log('Aprendiendo JavaScript');
}


//Arrow function la sintaxis es mas corta sustituye la palabra function por => y el parentesis cambia al lado izquierdo
const aprendiendo2 = () => 'Aprendiendo JavaScript';
console.log(aprendiendo2());

// una linea no requiere llaves


// retornar un valor
