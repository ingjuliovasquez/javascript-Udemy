
const aprendiendo = function(tecnologia, tecnologia2) {
    console.log(`Aprendiendo ${tecnologia} y ${tecnologia2}`);
}
aprendiendo('JavaScript', 'Node.js')



const aprendiendo2 = (tecnologia, tecnologia2) => `Aprendiendo $  {tecnologia} y ${tecnologia2}`; //se ocupa ´parentesis si son mas de un parametro
console.log(aprendiendo2('JavaScript', 'Node.js'));


/* si es un solo parmetro no ocupamos el parentesis
const aprendiendo = tecnologia => console.log(`aprendiendo ${tecnologia}`);
aprendiendo('JavaScript');*/