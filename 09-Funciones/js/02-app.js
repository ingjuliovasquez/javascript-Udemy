//Declaracion de Function ( Function Declaration )
sumar(); 
function sumar() { 
    console.log( 2 + 2);
} //esta sintaxis si va a funcionar aunque se este llamando la funcion antes de ejecutarla

// Eso pasa porque JavaScript se ejecuta digamos en 2 vueltas - Eso se le conoce como Hoisting , la primer vuelta registra todas las funciones y determina las variables, esta etapa se le llama de creación, 

// la segunda pasada es la que executa tu codigo, se llama de ejecución.

// Por lo tanto el primer código funciona porque function se registra primero y después se ejecuta el código.

// el segundo no funciona porque si bien es una función no es declarada como tal, lo ve más bien como una variable...

// Esto se le conoce como hosting que basicamente son esas 2 etapas (creación y ejecución)

// básicamente tu código se ejecuta asi:

const sumar2;
sumar2(); // a estas alturas es undefined...
sumar2 = function() {
    console.log(3 + 3); // pero como ya habiamos llamado la función, se queda como undefined
}



