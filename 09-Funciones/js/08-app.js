
function sumar(a, b) {
    return a + b ; //retorna un valor 
}

const resultado = sumar(2, 3); //es necesario tener una variable para que se le asigna el valor retornado

console.log(resultado);

//Ejemplo mas avanzado

let total = 0;
function agregarCarrito(precio) {
    return total += precio; //suma el precio de los elelmentos agregados al carrito
}

function calcularImpuesto(total) {
    return 1.15 * total; //se le suma el 1.5% de impuestos al valor del carrito
}

total = agregarCarrito(200);
total = agregarCarrito(300);
total = agregarCarrito(400);
console.log(total);


const totalPagar = calcularImpuesto(total); 

console.log(`El total a pagar es de ${totalPagar}`); //nos da el resulatdo