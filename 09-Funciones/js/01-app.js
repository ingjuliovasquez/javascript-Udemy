//Declaracion de Function ( Function Declaration )
function sumar() { //funtion es una palabra reservada es decir una variable no puede ser llamada function
    console.log( 2 + 2);
}

sumar();
sumar();
sumar(); //es reutilizable puede ser llamada muchas veces 



//Expresion de funcion ( Function Expression)
const sumar2 = function() {
    console.log(3 + 3); 
}

sumar2();