// Arrow function en map y forEach
const carrito = [
    { nombre: 'Monitor 27 pulgadas', precio: 500},
    { nombre: 'Television', precio: 100},
    { nombre: 'Tablet', precio: 200},
    { nombre: 'Audifonos', precio: 300},
    { nombre: 'Teclado', precio: 400},
    { nombre: 'Celular', precio: 700},
]


const nuevoArreglo = carrito.map( producto => `${producto.nombre} - Precio: ${producto.precio}` ); //no es necesario utilizar el return

carrito.forEach(producto => console.log( `${producto.nombre}- Precio: ${producto.precio}` ) ); //con la sintaxis es posible escribirlo en una sola linea

console.log(nuevoArreglo);