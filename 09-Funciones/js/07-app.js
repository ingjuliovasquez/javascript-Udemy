iniciarApp(); //Es mejor dividir una funcion partes pequeñas

function iniciarApp() {
    console.log('Iniciando app...');

    segundaFuncion();
}

function segundaFuncion() {
    console.log('Desde la segunda funcion');

    usuarioAutenticado('Pablo');
}

function usuarioAutenticado(usuario){
    console.log('Autoenticando usuario... espere...');
    console.log(`Usuario autoenticado exitosamente ${usuario}`); //toma el nombre de la funcion 'usuarioAutenticado'
}
//se puede mandar a llamar una funcion dentro de otra 