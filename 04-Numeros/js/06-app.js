const numero1 = "20";
const numero2 = "20.2"; //numero es un string
const numero3 = "Uno";
const numero4 = 20.2;

console.log(numero1);
console.log(Number.parseInt(numero1)); //lo convierte a un numero entero
console.log(Number.parseFloat(numero2)); //lo convierte a un numero con una fraccion
console.log(Number.parseInt(numero3)); //indica que no es un numero


//Revisar si un numero es entero o no
console.log(Number.isInteger(numero4)); 
console.log(Number.isInteger(numero3));