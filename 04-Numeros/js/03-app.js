
let resultado;

//Pi
resultado = Math.PI; //llama el valor de PI

//Redondear
resultado = Math.round(2.8);
resultado = Math.round(2.2);
resultado = Math.round(2.6);
resultado = Math.round(2.5);
resultado = Math.round(2.4);

//Siempre redondear hacia arriba
resultado = Math.ceil(2.1);

//Redondea hacia abajo 
resultado = Math.floor(2.9);

//Para obtener la raiz cuadrada 
resultado = Math.sqrt(144);

//Absoluta
resultado = Math.abs(-500); //ignora el signo negativo y solo toma en cuenta el numero

//Potencia 
resultado = Math.pow(2, 4);

//Minimo 
resultado = Math.min(3, 5, 1, 12, -3);

//Maximo
resultado = Math.max(3, 5, 1, 12, -3);

//Aleatorio dentro de un trango de numero
resultado = Math.floor( Math.random() * 30);
//busca un numero random que va del nuemero 0 a 30 es util para utilizarlo en una rifa


console.log(resultado);
