const carrito = document.querySelector('#carrito');
const contenedorCarrito = document.querySelector('#lista-carrito tbody');
const vaciarCarritoBtn = document.querySelector('#vaciar-carrito');
const listaCursos = document.querySelector('#lista-cursos'); // SElecciona los elemntos sobre los que vamos a trabajar
const notificacionCarrito = document.getElementById('notificacion-carrito')
let articulosCarrito =[]; //en un principio el carrito esta vacio
añadirNotificacion(articulosCarrito.length);
cargarEventListeners(); //manda a llamar la funcion


function cargarEventListeners() {
    //cuando agregas un curso presionando "Agregar al Carrito"
    listaCursos.addEventListener('click', agregarCurso) //se agregar un nuevo curso al carrito

    //ELIMINAR CURSOS DEL CARRITO
    carrito.addEventListener('click', eliminarCurso); //cuando el usuario haga click se ejecutara la funcion eliminar el curso 

    vaciarCarritoBtn.addEventListener('click', () => { //actua sobre el boton de vaciar carrito 
        articulosCarrito = [];
        limpiarHTML(); //limpia el html 
        añadirNotificacion(articulosCarrito.length);
    });
}

//Funciones 
function agregarCurso(e) { //cuando se agrega un curso se ejecuta esta funcion 
    e.preventDefault();


    if(e.target.classList.contains('agregar-carrito')) { //nos aseguramos de que el usuario haya presionado 'agregar carrito'
        const cursoSeleccionado = e.target.parentElement.parentElement; //se agrega a el carrito todo el div que contiene la informacion del curso 

        leerDatosCurso(cursoSeleccionado);
    } 
    añadirNotificacion(articulosCarrito.length)
}
function añadirNotificacion(cantidad) {
    if (cantidad !== 0) {
        notificacionCarrito.dataset.cantidadnotificacion = articulosCarrito.length
        notificacionCarrito.classList.add('notificacion-carrito--visible')
    } else if (cantidad === 0) {
        notificacionCarrito.classList.remove('notificacion-carrito--visible')
    }
}
//ELIMINAR UN CURSO DEL CARRITO por el data-id
function eliminarCurso(e) { //elimina los cursos de la lista de productos agregados a la lista de carrito
    if(e.target.classList.contains('borrar-curso')) {
        const cursoId = e.target.getAttribute('data-id');

        //elimina del arreglo de articulosCarrito por el data-id
        articulosCarrito = articulosCarrito.filter( curso => curso.id !== cursoId);
        añadirNotificacion(articulosCarrito.length)
        carritoHTML(); //iterar sobre el carrito y mostrar su HTML
    }
}

//Lee el contenido del HTML al que le dimos click y extrae la informacion del curso
function leerDatosCurso(curso) { //leemos los datos 
    //console.log(curso) //llama a parentElement


    //Crear un objeto con el contenido del curso actual
    const infoCurso = {
        imagen: curso.querySelector('img').src, //nos permite acceder a la imagen
        titulo: curso.querySelector('h4').textContent, //extrae el texto del h4
        precio: curso.querySelector('.precio span').textContent, //nos permite extraer de la clase precio el span es decir el descuento
        id:curso.querySelector('a').getAttribute('data-id'), //nis dice que numero de curso se agrego al carrito
        cantidad: 1
    }

    //REVISA SI UN ELEMENTO YA EXISTE EN EL CARRITO
    const existe = articulosCarrito.some( curso => curso.id === infoCurso.id );
    if(existe) {
        //ACTUALIZAMOS LA CANTIDAD
        const cursos = articulosCarrito.map( curso => {
            if( curso.id === infoCurso.id ) {
                curso.cantidad++; //suma uno em umo la cantidD DE VECES QUE UN CURSO SE AGREGO AL CARRITO
                return curso; //retorna el objeto actualizado 
            } else {
                return curso; //retorna los objetos que no son los duplicados 
            }
        });
        articulosCarrito = [...cursos];
    } else { //hace que no se duplique la informacion  solo la cantidad
        //AGREGAR ELEMENTOS AL ARREGLO DE CARRITO
        articulosCarrito = [...articulosCarrito, infoCurso]; //copia el cariito vacio para ir agregandolo diferentes articulos sin que se pierda el anterior 
    }

    

    //console.log(articulosCarrito);

    carritoHTML(); //   APARECE EN FORMA DE LISTGA EN EL ICONO DE CARRITO
    
}
//MUESTRA EL CARRITO DE COMPRAS EN EL HTML
function carritoHTML() {

    //LIMPIAR EL HTML PARA EVITAR DUPLICADOS
    limpiarHTML();

    //RECORRE EL CARRITO Y GENERA UN HTML
    articulosCarrito.forEach( curso => {
        //console.log(curso)
        const { imagen, titulo, precio, cantidad, id} = curso; //se puede tener una sintaxis en una sola linea
        const row = document.createElement('tr');
        row.innerHTML = `
            <td> 
                <img src="${curso.imagen}" width="100"/> 
            </td> 
            <td>${titulo}</td>
            <td>${precio}</td>
            <td>${cantidad}</td>
            <td>
               <a href="#" class="borrar-curso" data-id="${curso.id}"> X </a>
            </td>
        `;
//podemos modificar el tamaño de la imagencon width
        //  AGREGAR EL HTML DEL CARRITO
        contenedorCarrito.appendChild(row);
    }); 
}

//ELIMINAR LOS CURSOS DEL TBODY
function limpiarHTML() {
    //FORMA LENTA 
   // contenedorCarrito.innerHTML = '';

   while(contenedorCarrito.firstChild) { //mientras que la condicion se cumpla el elemento padre va a eliminar un hijo por el primero
        contenedorCarrito.removeChild(contenedorCarrito.firstChild);
   }
}