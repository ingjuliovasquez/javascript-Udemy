function iniciarApp() {

    const resultado = document.querySelector('#resultado');

    const selectCategorias = document.querySelector('#categorias'); //selecionamos el id de categorias 

    if(selectCategorias) { //si existe select se ejecutara el siguiente codigo
        selectCategorias.addEventListener('change', seleccionarCategoria) //change se utiliza comunmente en un Select 
        obtenerCategorias();
    }
    const favoritosDiv = document.querySelector('.favoritos') //seleccionamos la clase
    if(favoritosDiv) {
        obtenerFavoritos(); 
    }
    
    const modal = new bootstrap.Modal('#modal', {});

    function obtenerCategorias() {
        const url = 'https://www.themealdb.com/api/json/v1/1/categories.php'
        fetch(url) //es un llamado a la URL
            .then(respuesta => {
                return respuesta.json() //utilizaremos el formato de tipo json
            })
            .then (resultado => {
                mostrarCategorias(resultado.categories)
            })
    }

    function mostrarCategorias(categorias = []) {
        categorias.forEach( categoria => { //accedemos a casa una de las categorias 
            const { strCategory } = categoria; //destructuring tambien es util para evitar repetir codigo
            const option = document.createElement('OPTION')
            option.value = strCategory;
            option.textContent = strCategory;
            selectCategorias.appendChild(option) //agrega las categorias al selec
        })
    }

    function seleccionarCategoria(e) { //seleciona los platillos que estan relacionados con esa categoria 
        const categoria = e.target.value;
        const url = `https://www.themealdb.com/api/json/v1/1/filter.php?c=${categoria}`;

        fetch(url) //selecciona las recetas que tengan que ver con la categoria que seleccionamos 
            .then(respuesta => respuesta.json()) 
            .then(resultado => mostrarRecetas(resultado.meals))
    }

    function mostrarRecetas( recetas = []) { //creamos un contenedor y card para mostrar las recetas relacionadas con la categoria que estamos seleccionando

        limpiarHTML(resultado);

        const heading = document.createElement('H2');
        heading.classList.add('text-center', 'text-black', 'my-5');
        heading.textContent = recetas.length ? 'Resultados': 'No hay resultados'; //? es un if, : es una negacion 
        resultado.appendChild(heading);

        //iterar en cada uno de los resultados 
        recetas.forEach(receta => {
            const { idMeal, strMeal, strMealThumb} = receta;

            const recetaContenedor = document.createElement('DIV')
            recetaContenedor.classList.add('col-md-4');
    
            const recetaCard = document.createElement('DIV');
            recetaCard.classList.add('card', 'mb-4');

            const recetaImagen = document.createElement('IMG');
            recetaImagen.classList.add('card-img-top');
            recetaImagen.alt = `Imagen de la receta ${strMeal} ?? receta,titulo`;
            recetaImagen.src = strMealThumb ?? receta.img;

            const recetaCarBody = document.createElement('DIV');
            recetaCarBody.classList.add('card-body');

            const recetaHeading = document.createElement('H3');
            recetaHeading.classList.add('card-title', 'mb-3');
            recetaHeading.textContent = strMeal ?? receta.titulo;

            const recetaButton = document.createElement('BUTTON');
            recetaButton.classList.add('btn', 'btn-danger', 'w-100');
            recetaButton.textContent = 'Ver Receta';
            //recetaButton.dataset.bsTarget = "#modal";//conectamos el id de modal
            //recetaButton.dataset.bsToggle = "modal"; //manda a llamar las funciones que estan en el archivo de bustrap y le se lo agregamos al boton
            recetaButton.onclick = function() { //se ejecuta la funcion cuando ocurre el evento de onclick
                seleccionarReceta(idMeal ?? receta.id)
            }

            //inyectar en el codigo HTML 
            recetaCarBody.appendChild(recetaHeading);
            recetaCarBody.appendChild(recetaButton);

            recetaCard.appendChild(recetaImagen);
            recetaCard.appendChild(recetaCarBody);

            recetaContenedor.appendChild(recetaCard);

            resultado.appendChild(recetaContenedor);

        })
    }

    function seleccionarReceta(id) {
        const url = `https://themealdb.com/api/json/v1/1/lookup.php?i=${id}`;
        fetch(url)
            .then(respuesta => respuesta.json())//recibimos la respuesta de tipo json 
            .then(resultado => mostrarRecetaModal(resultado.meals[0])); //convierte el resultado de arreglo a objeto 
    }

    function mostrarRecetaModal(receta) {
        const {idMeal, strInstructions, strMeal, strMealThumb } = receta;

        //añadir contenido al modal
        const modalTitle = document.querySelector('.modal .modal-title'); //seleccionamos las clases 
        const modalBody = document.querySelector('.modal .modal-body');

        modalTitle.textContent = strMeal;
        //innerHTML se usa siempre y cuando se sepa de donde vienen los datos no es recomendable cuando un usuario llena un formulario
        modalBody.innerHTML = `
        <img class="img-fluid" src="${strMealThumb}" alt="receta ${strMeal}" />
        <h3 class="my-3">Instrucciones</h3>
        <p>${strInstructions}</p>
        <h3 class="my-3">Ingredientes y Cantidades</h3>
        `;

        const listGroup = document.createElement('UL'); //enlista los ingredientes 
        listGroup.classList.add('list-group');

        //Mostrar cantidades e ingredientes 
        for(let i = 1; i <= 20; i++ ) { //iniciamos desde 1 y vamso incrementando de uno en uno hasta llegar a un numero menor a 20 
            if(receta[`strIngredient${i}`]) {//accedemos a cada uno de los ingredientes 
                const ingrediente = receta[`strIngredient${i}`]; 
                const cantidad = receta[`strMeasure${i}`]; 

                const ingredienteLi = document.createElement('LI');
                ingredienteLi.classList.add('list-group-item');
                ingredienteLi.textContent = `${ingrediente} - ${cantidad}`;

                listGroup.appendChild(ingredienteLi)
            } //es igual a ingrediente 1 y cantidad 1 y asi sucecivamente 
        } 
        modalBody.appendChild(listGroup);

        const modalFooter = document.querySelector('.modal-footer') //selecionamos la clase para despues agregar el boton 
        limpiarHTML(modalFooter); //evita que se agreguen mas botones 

        //Botones de Cerrar y Favorito
        const btnFavorito = document.createElement('BUTTON'); //creamos el boton
        btnFavorito.classList.add('btn', 'btn-danger', 'col'); //agregamos clases 
        btnFavorito.textContent = existeStorage(idMeal) ? 'Eliminar Favorito' : 'Guardar Favorito'; //si yaexiste en localStorage aparecer el boton 'eliminar favorito' y si no 'guardar favorito'

        btnFavorito.onclick = function() {

            if(existeStorage(idMeal)) { //si ya existe en localStorage el return no va adejar que se ejecuten las siguientes lineas 
                eliminarFavorito(idMeal); //seleccionamos el id que queremos eliminar 
                btnFavorito.textContent = 'Guardar Favorito'
                mostrarToast('Eliminado correctamente'); //mensaje de confirmacion 
                return
            }
            agregarFavorito({
                id: idMeal, 
                titulo: strMeal,
                img: strMealThumb
            })
            btnFavorito.textContent = 'Eliminar Favorito';
            mostrarToast('Agregado correctamente');
        }

        const btnCerrarModal = document.createElement('BUTTON');
        btnCerrarModal.classList.add('btn', 'btn-secondary', 'col');
        btnCerrarModal.textContent = 'Cerrar';
        btnCerrarModal.onclick = function() { //ejecuta al funcion despues de que se ejecute el evento de onclick
            modal.hide();
        }

        modalFooter.appendChild(btnFavorito);//agreagmos el boton al final del footer 
        modalFooter.appendChild(btnCerrarModal);

        //mostrar el modal 
        modal.show();
    }

    function agregarFavorito(receta) {
        const favoritos = JSON.parse(localStorage.getItem('favoritos')) ?? []; //?? en caso de que en el primero se marque null va aplicar el lado derecho 
        localStorage.setItem('favoritos', JSON.stringify([...favoritos, receta])) //agregamos favoritos al localStorage
    }

    function eliminarFavorito(id) {

        const favoritos = JSON.parse(localStorage.getItem('favoritos')) ?? [];
        const nuevosFavoritos = favoritos.filter(favorito => favorito.id !== id); //filtra todos los id que sean diferentes al que le estamos pasando  
        localStorage.setItem('favoritos', JSON.stringify(nuevosFavoritos)); //se actuliza localStorge

    }

    function existeStorage(id) { //hace que solo se agregue una vez las recetas
        const favoritos = JSON.parse(localStorage.getItem('favoritos')) ?? [];
        return favoritos.some(favorito => favorito.id === id); //some itera sobre cada uno de lo sarreglos y retorna si al menos uno cuenta con la condicion y despues se le pasa un id 
    }

    function mostrarToast(mensaje) { //toma un mensaje de referencia
       const toastDiv = document.querySelector('#toast') //selecinamos el id 
       const toastBody = document.querySelector('.toast-body') //seleccionamos la clase 
       const toast = new bootstrap.Toast(toastDiv); //se genera en el div
       toastBody.textContent = mensaje; //aparecera el mensaje cuando mandemos a llamar la funcion 
       toast.show(); //lo mostramos 
    }

    function obtenerFavoritos() {
        const favoritos = JSON.parse(localStorage.getItem('favoritos')) ?? [];

        if(favoritos.length) { //se ejecutara el codigo en caso de que haya algo en algo
            mostrarRecetas(favoritos);
            return
        }

        const noFavoritos = document.createElement('P');
        noFavoritos.textContent = 'No hay favoritos aún';
        noFavoritos.classList.add('fs-4', 'text-center', 'font-bold', 'mt-5');
        favoritosDiv.appendChild(noFavoritos);

    }
    function limpiarHTML(selector) { //el parametro de selector lo hace mas dinamico y accesible a eliminar cualquier otro parametro
        while(selector.firstChild) {
            selector.removeChild(selector .firstChild); //si hay un resultado remueve el anterior 
        }
    }
}
document.addEventListener('DOMContentLoaded', iniciarApp)