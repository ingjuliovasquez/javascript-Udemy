const carrito = [ //arreglo de objetos
    { nombre: 'Monitor 27 Pulgadas', precio: 500 },
    { nombre: 'Televisión', precio: 100 },
    { nombre: 'Tablet', precio: 100 },
    { nombre: 'Audifonos', precio: 300 },
    { nombre: 'Teclado', precio: 400 },
    { nombre: 'Celular', precio: 700 },
] 

let resultado; 

resultado = carrito.filter( producto => producto.precio > 400); //crea un nuevo arreglo que enlista los productos mayores al valor asignado en este caso es 400

resultado1 = carrito.filter( producto => producto.precio  < 600 ); //todos los que sean menor a 600
resultado2 = carrito.filter( producto => producto.nombre === 'Celular'  ); // Traerte el celular
resultado3 = carrito.filter(producto => producto.nombre !== 'Celular'); // Todos menos celular

console.log(resultado); 
console.log(resultado1); 
console.log(resultado2); 
console.log(resultado3); 