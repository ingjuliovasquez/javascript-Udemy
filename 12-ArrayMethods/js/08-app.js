const meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio']; //arreglo de indices 

const carrito = [ //arreglo de objetos
    { nombre: 'Monitor 27 Pulgadas', precio: 500 },
    { nombre: 'Televisión', precio: 100 },
    { nombre: 'Tablet', precio: 200 },
    { nombre: 'Audifonos', precio: 300 },
    { nombre: 'Teclado', precio: 400 },
    { nombre: 'Celular', precio: 700 },
]

//spread operator con arreglo de indices
const meses2 = ['Agosto', ...meses];
console.log(meses2); //el valor tiene que pasarse como string para evitar que en la consola se ejecute letra por letra

const producto = { nombre: 'Disco Duro', precio: 300};
const carrito2 = [producto, ...carrito]; //crea un nuevo producto y lo agrega al principio del carrito porque asi esta acomodado

console.log(carrito2);