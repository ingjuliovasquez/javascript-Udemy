const meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio']; //arreglo de indices 

const carrito = [ //arreglo de objetos
    { nombre: 'Monitor 27 Pulgadas', precio: 500 },
    { nombre: 'Televisión', precio: 100 },
    { nombre: 'Tablet', precio: 200 },
    { nombre: 'Audifonos', precio: 300 },
    { nombre: 'Teclado', precio: 400 },
    { nombre: 'Celular', precio: 700 },
]

//comprobar si un valor existe en un arreglo

/*meses.forEach (mes => {
    if(mes === 'Enero') {
        console.log('Enero si existe')
    }
})*/

/*const resultado = meses.includes('Diciembre');
console.log(resultado) //MARCA FALSE PORQUE DICIEMBRE NO ESTA EN LA LISTA DE OBJETOS*/

//EN UN ARREGLO DE OBJETOS SE UTILIZA .some PARA SABER SI EXISTE UN OBJETO
const  existe = carrito.some( producto =>  producto.nombre === 'Celular')

console.log(existe);

const existe2 = meses.some( mes => mes === 'Febrero')
console.log(existe2)