const meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio'];
const meses2 = ['Agosto', 'Septiembre', 'Octubre'];
const meses3 = ['Noviembre', 'Diciembre'];

//   .concat 
const resultado = meses.concat(meses3, meses2);
console.log(resultado); // se unen los meses solo separandolos con una coma


//spread operator
const resultado2 = [...meses, meses2, ...meses3];
console.log(resultado2); //los 3 puntos significan que se estan copiando los arreglos haciendo que los resulatdos se unan