const carrito = [ //arreglo de objetos
    { nombre: 'Monitor 27 Pulgadas', precio: 500 },
    { nombre: 'Televisión', precio: 100 },
    { nombre: 'Tablet', precio: 100 },
    { nombre: 'Audifonos', precio: 300 },
    { nombre: 'Teclado', precio: 400 },
    { nombre: 'Celular', precio: 700 },
] 

//CON UN forEach 
let total = 0;
carrito.forEach( producto => total += producto.precio );
console.log(total); //sume los numero del carrito, es decir, el precio


//con un reduce 
let resultado = carrito.reduce( ( total, producto) => total + producto.precio, 0); //tambien suma los resultados pero se le da un valor para poder empezar en este caso empieza a contar desde 0
console.log(resultado);