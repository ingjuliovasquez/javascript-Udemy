const carrito = [ //arreglo de objetos
    { nombre: 'Monitor 27 Pulgadas', precio: 500 },
    { nombre: 'Televisión', precio: 100 },
    { nombre: 'Tablet', precio: 100 },
    { nombre: 'Audifonos', precio: 300 },
    { nombre: 'Teclado', precio: 400 },
    { nombre: 'Celular', precio: 700 },
];

const resultado = carrito.every( producto => producto.precio < 500); //todos los elementos deben cumplir con la condicion en este caso no se cumple por lo tanto es false 
console.log(resultado); //parece al &&

const resultado2 = carrito.some( producto => producto.precio < 500); //revisa que al menos una condicion se cumpla 
console.log(resultado2) // aperecion al || (or)