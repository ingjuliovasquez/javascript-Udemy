const meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio']; //arreglo de indices 

const carrito = [ //arreglo de objetos
    { nombre: 'Monitor 27 Pulgadas', precio: 500 },
    { nombre: 'Televisión', precio: 100 },
    { nombre: 'Tablet', precio: 100 },
    { nombre: 'Audifonos', precio: 300 },
    { nombre: 'Teclado', precio: 400 },
    { nombre: 'Celular', precio: 700 },
]

meses.forEach( (mes, i) => { //i signidfica indice
    if( mes === 'Abril') {
        console.log(`Encontrado en el indice ${i}`)
    } // nos dice en donde esta posicionado 
})

//Encontrar el indice de Abril
//hace la mima funcion que el codigo anterior pero con una sintaxis mas corta
const indice = meses.findIndex( mes => mes === 'Abril');
console.log(indice); //en caso de que no enciuentre algun nombre como por ejemplo 'Diciembre' marcara un -1 que significa que es un error

//ENCONTRAR UN INDICE DE UN ARREGLO DE OBJETOS...
const indice2 = carrito.findIndex( producto => producto.precio === 100); //NOS PERMITE SABER QUE ELEMENTOS CUESTAN 100 EN ESTE CASO AUNQUE SOLO SON DOS SOLO MARCARA EL PRIMERO QUE ENCUENTRA
console.log(indice2);
 