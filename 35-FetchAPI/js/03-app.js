//Un JSON en array se ve representado con corchetes porque tiene muchos 

const cargarJSONArrayBtn = document.querySelector('#cargarJSONArray') //seleccionamos el id 
cargarJSONArrayBtn.addEventListener('click', obtenerDatos) //se ejecuta la funcion cuando se presenta el evento de click

function obtenerDatos() {
    const url = 'data/empleados.json'; //creamos una variable con la ruta del 

    fetch(url) //seleccionamos el json 
        .then(respuesta => respuesta.json())
        .then(resultado => mostrarHTML(resultado))
}

function mostrarHTML(empleados) {
    const contenido = document.querySelector('.contenido'); //seleccionamos la clase de contenido 

    let html = ''; //variable vacia modificable

    empleados.forEach(empleado => { //iteramos en cada uno de los elmentos 
        const { id, nombre, empresa, trabajo } = empleado; //destraructuring

        //concatenamos todo el html
        html += `
        <p>Empleado: ${nombre}</p>
        <p>ID: ${id}</p>
        <p>Empresa: ${empresa}</p>
        <p>Trabajo: ${trabajo}</p>
        `;
    });

    contenido.innerHTML = html; //agregamos el contenido a html generando una nueva pagina con llos datos del empleado
}