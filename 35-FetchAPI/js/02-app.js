const cargarJSONBtn = document.querySelector('#cargarJSON') //seleccipnamos el id 

cargarJSONBtn.addEventListener('click', obtenerDatos);

function obtenerDatos() {
    const url = 'data/empleado.json' //le decimos en donde queremos trabajar 
    fetch(url) //obtiene los datos desde un archivo json
        .then( respuesta => respuesta.json() )
        .then( resultado => mostrarHTML(resultado))
}s

function mostrarHTML({empresa, id, nombre, trabajo}) {
    const contenido = document.querySelector('.contenido');

    //crea una nueva ventana con la informacion creada con parrafos 
    contenido.innerHTML = `
    <p>Empleado: ${nombre}</p>
    <p>ID: ${id}</p>
    <p>Empresa: ${empresa}</p>
    <p>Trabajo: ${trabajo}</p>
    `
}