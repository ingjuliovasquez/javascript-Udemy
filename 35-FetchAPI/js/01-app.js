const cargarTxtBtn = document.querySelector('#cargarTxt'); //seleccionamos el id 
cargarTxtBtn.addEventListener('click', obtenerDatos) //cuando se presente el evento de click se ejecutara la funcion 

function obtenerDatos() {
    const url = 'data/datos.txt';

    fetch(url) //consultamos la url 
    .then(respuesta => {
        //valores que podemos obtener 
        console.log(respuesta);
        console.log(respuesta.status);
        console.log(respuesta.statusText);
        console.log(respuesta.url);
        console.log(respuesta.type);

        return respuesta.text();//retorna una respuesta en forma de texto
    })
    .then( datos => 
        console.log(datos) //imprime el contenido de la respuesta
    )
    
    //en casd¿o de que alla un error en la consulta 
    .catch( error => {
        console.log(error)
    })
}