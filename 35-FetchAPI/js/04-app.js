//cargar Api desde una url
const cargarAPIBtn = document.querySelector('#cargarAPI'); //seleccionamos el id sobre el que vamos a trabjar 
cargarAPIBtn.addEventListener('click', obtenerDatos); //al dar click em el id que previamente ya le pusimos el nombre de cargarAPIBtn se ejecutara la funcion de obtnerDatos 

function obtenerDatos() {
    const url = 'https://picsum.photos/list'; 
    fetch(url) //asignamos el url a fetch
        .then(respuesta => respuesta.json())  //obtenemos la respuesta del json
        .then(resultado => mostrarHTML(resultado)); //asignamos el resultado a la funcion de mostrarHTML
}
function mostrarHTML(datos) {

    const contenido = document.querySelector('.contenido') //seleccionamos la clase de contenido 
    let html = ''; //variable vacia }
    datos.forEach(perfil => {
        const {autor, post_url } = perfil;
        html += `
        <p>Autor: ${autor}</p>
        <a href="${post_url}" target"_black">Ver Imagen</a>
        `
    });

    contenido.innerHTML = html; //la  variable se agrega al contenido ara que aparezca una pagina con cada una de las imagenes al pulsar el boton de enlace 
}