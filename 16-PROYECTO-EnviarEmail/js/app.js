document.addEventListener('DOMContentLoaded', function() {

    const email = { //arreglo de objetos que enlista los campos que deben contener informacion obligatoria
        email: '',
        asunto: '',
        mensaje: ''
    }
    //Seleccionar los elementos de la interfaz 
    const inputEmail = document.querySelector ('#email');
    const inputAsunto = document.querySelector ('#asunto');
    const inputMensaje = document.querySelector ('#mensaje');
    const formulario = document.querySelector('#formulario');
    const btnSubmit = document.querySelector('#formulario button[type="submit"]'); //selecciona een el id de formulario el atributo de tipo submit
    const btnReset = document.querySelector('#formulario button[type="reset"]'); //selecciona een el id de formulario el atributo de tipo reset 
    const spinner = document.querySelector('#spinner');
    const inputCc = document.querySelector('#cc')

    //Asignar eventos 
    inputEmail.addEventListener('input', validar);
    inputCc.addEventListener('input', validarCc)
    inputAsunto.addEventListener('input', validar);
    inputMensaje.addEventListener('input', validar); //input da una experiencia en tiempo real es decir nos permite ver a las alertas a medida que vamos escribiendo, eso nos ayuda aver en que momento se han llenado los campos corectamente
    formulario.addEventListener('submit', enviarEmail);

    btnReset.addEventListener('click', function(e) {
        e.preventDefault(); //evita que haga la funcion por default

        resetFormulario();
    
        formulario.reset();
        comprobarEmail(); //al resetear el formulario elimina el background del boton enviar
    })

    function enviarEmail(e) {
        e.preventDefault();

        spinner.classList.add('flex'); //agrega la clase flex
        spinner.classList.remove('hidden'); //elimina la clase hidden 

        setTimeout(() => {
            spinner.classList.remove('flex'); 
            spinner.classList.add('hidden'); 

            // se reinicia el formulario y se deshabilita el boton de enviar 
            resetFormulario();

            //Crea UNA ALERTA DESPUES DE QUE SE ENVIO EL FORMULARIO EXITOSAMENTE
            const alertaExito = document.createElement('P');
            alertaExito.classList.add('bg-green-500', 'text-white', 'p-2', 'text-center', 'reounder-lg', 'mt-10', 'font-bold', 'text-sm', 'uppercase');

            alertaExito.textContent ='El mensaje fue enviado correctamente'; 
            formulario.appendChild(alertaExito); //inserta la alerta al final del formulario
        
            setTimeout(() => {
                alertaExito.remove();
            }, 3000) //hace que desaparezca la alerta de envio exitoso despues de 3 segundos
        
        }, 3000); //significa 3000 milisegundos es decir despues de 3 segundos va a desaparecer el spinner haciendo una simulacion de que se envio el formulario

    }

    function validar (e) {
        if(e.target.value.trim() === '') { //trim verifica que no haya espacios vacios de mas 
            mostrarAlerta(`El campo ${e.target.id} es obligatorio`, e.target.parentElement); //se cre el mensaje y PARENTELEMENT hace que vayamos un nivel mas arriba que el input, haciendo que el mensaje de alerta aparesca en la parte de abajo de cada input
            email[e.target.name] = ''; //reiniviamos y despues comprobamos
            comprobarEmail() //se asegura de que el campo este bien completado y sea valido
            return; //detiene la ejecucion del codigo
        } 
        if(e.target.id === 'email' && !validarEmail(e.target.value)) { //que se cumplan ambas condiciones 
            mostrarAlerta('El email no es válido', e.target.parentElement);
            email[e.target.name] = '';
            comprobarEmail();//se asegura de que el campo este bien completado y sea valido
            return;
        }
        limpiarAlerta(e.target.parentElement);

        //ASIGNAR LOS VALORES 
        email[e.target.name] = e.target.value.trim().toLowerCase(); //se asegura que el el email elimine los espacios y se pase en automatico todo en minusculas 

        //COMPROBAR EL OBJETO DE EMAIL
        comprobarEmail();
    }

    function mostrarAlerta(mensaje, referencia) { //FUNCION REUTILIZABLE
        limpiarAlerta(referencia); 
        //Crea un ALERTA en el HTML 
        const error = document.createElement('P') //se recomienda que el elemnto gcreado se ponga en mayusculas
        error.textContent = mensaje; //
        error.classList.add('bg-red-600', 'text-white', 'p-2', 'text-center'); //se agregan estilos a la alerta es decir, se le aplica un background rojo, texto blanco, padding de 2 pixeles, texto centrado

        //aplica el eroor en el html
        referencia.appendChild(error); //appendChild crea un hijo en lo existente en este caso crea un parrafo al final de cada input
    }
    function limpiarAlerta(referencia) {
        //COMPROBBAR SI EXISTE UNA ALERTA 
        const alerta = referencia.querySelector('.bg-red-600'); //referencia busca la clase .bag-red-600 en cada div
        if(alerta) {
            alerta.remove()
        }
    }
    function validarEmail(email) {
        const regex =  /^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/ //codigo que se cumpla con las caracteristicas que componen un email
        const resultado = regex.test(email);
        return resultado; //una vez que el email esta bien escrito no vulve aparecer la alerta
    }

    function comprobarEmail() {
        if(Object.values(email).includes('')) {// values toma los valores de emali crea un nuevo arreglo con los valores de ese objeto, includes identifica si es un string vacio o no
            btnSubmit.classList.add('opacity-50');
            //remueve la clase de opacity cuando el formulario ya esta listo para enviar
            btnSubmit.disabled = true;
            return
        }  
        btnSubmit.classList.remove('opacity-50'); //remueve la clase de opacity cuando el formulario ya esta listo para enviar
        btnSubmit.disabled = false;
    }

    function resetFormulario() {
        //reiniciar el objeto
        email.email = '';
        email.cc = '';
        email.asunto = '';
        email.mensaje = '';
    
        formulario.reset();
        comprobarEmail(); 
    }
    function validarCc(e){
        email[e.target.name] = e.target.value.trim().toLowerCase(); //se asegura de que todo este en minusculas

        if(!validarEmail(e.target.value)){ //si el email no es valido aparecera la siguiente alerta 
            mostrarAlerta('El email no es válido', e.target.parentElement);
            email[e.target.name] = ''; //reiniciamos
            comprobarEmail(); //comprobamos
        }else{
            limpiarAlerta(e.target.parentElement); //si el email si es valido se limpia la alerta 
            comprobarEmail(); //comprobamos 
        };

        if(e.target.value === ''){
            delete email.cc;
            limpiarAlerta(e.target.parentElement);
            comprobarEmail();
            return;
        };
    };
}); 