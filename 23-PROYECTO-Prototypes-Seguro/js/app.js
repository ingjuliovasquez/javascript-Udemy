
//CONSTRUCTORES
function Seguro(marca, year, tipo) {//crea una nueva estancia de seguro y mostrara la estructura que tendria el objeto 
    this.marca = marca;
    this.year = year;
    this.tipo = tipo;
}

//REALIZAR LA COTIZACION CON LOS DATOS
Seguro.prototype.cotizarSeguro = function() { //funcion unica de seguro
    /* 
        1 = Americano 1.15
        2 = Asiatico 1.05
        3 = Europeo 1.35
    */

    let cantidad;
    const base = 2000; //es un valor sirve de vaso para multiplicar el valor de cada auto
    
    switch(this.marca) {//revisa multiples condiciones
        case '1': //es el id de americano
            cantidad = base * 1.15; //se multiplica por el valor del auto
            break;
        case '2': //es el id de asiatico
            cantidad = base * 1.05;
            break;
        case '3': //es el id de europeo
            cantidad = base * 1.35;
            break;
    }

    //leer el año
    const diferencia = new Date().getFullYear() - this.year; //toma de base el año actual y le resta el año que el usuario escoja 

    //cada año que la diferencia es mayor, el costo va a reducir un 3%
    cantidad -= ((diferencia * 3) * cantidad) / 100; 

    /* 
        si el seguro es basico se ,ultiplica por un 30% mas 
        si el seguro es completo se multtiplica por un 50%
    */

    if(this.tipo === 'basico') {
        cantidad *= 1.30;
    } else {
        cantidad *= 1.50;
    }
    return cantidad;
}
function UI() {} //no requiere datos porque los datos se van a ir generando de acuerdo a lo que vaya sucediendo en la aplicacion


//LLENA LAS OPCIONES DE LOS AÑOS
UI.prototype.llenarOpciones = () => { //genera un prototype unico de UI
    const max = new Date().getFullYear(), //toma como referencia el año actual
        min = max - 20; //año actualmenos 20

    const selectYear = document.querySelector('#year');
    for(let i = max; i > min; i--) { //i es igual al año actual, mientras i sea mayor que el año minimo, se restaran los años de uno en uno
        let option = document.createElement('option'); //creamo un option
        option.value = i; //value es igual al año actual
        option.textContent = i; //hace que los años aparescan en el html
        selectYear.appendChild(option); //agrega las opciones al final 
    }
}

//  MOSTRAR ALERTAS EN EL HTML
UI.prototype.mostrarMensaje = (mensaje, tipo) => { //definimos una funcion unica de ui que molstrara un mensaje cuando el formulario este completo o no 
    //los argumentos nos indican que tomamos el mesnjase y el tipo mde mensaje 

    const div = document.createElement('div'); //creamos un nuevo div

    if(tipo === 'error') { //si el formulario esta incorrecto aparcera un mensaje con la clase 'error'
        div.classList.add('error');
    } else {//si el formulario esta correcto aparcera un mensaje con la clase 'correcto'
        div.classList.add('correcto');
    }
    div.classList.add('mensaje', 'mt-10'); //las clases pueden ir afuera para que se apliquen al error y al correcto
    div.textContent = mensaje;

    //insertar en el html se puede repetir porque esta en diferente funcion
    const formulario = document.querySelector('#cotizar-seguro'); //selecciona y valioda el id de cotizar-seguro
    formulario.insertBefore(div, document.querySelector('#resultado')); //insertamos un div antes del id de resultado 

    setTimeout(() => { //despues de 3 segundos se elimina la alerta 
        div.remove();
    }, 3000);
}

UI.prototype.mostrarResultado = (total, seguro) => {

    //destructuring
    const {marca, year, tipo} = seguro;

    let textoMarca; //usamos let para poder reasignar la variable

    switch(marca) { //revisa multiples condiciones nos da el nombre del modelo en vdes de solo el numero del id
        case '1':
            textoMarca = 'Americano';
            break;
        case '2':
            textoMarca = 'Asiatico';
            break;
        case '3':
            textoMarca = 'Europeo';
            break;
        default: //lleva dos puntos NO punto y coma
            break;
    }

    //CREAR EL RESULTADO 
    const div = document.createElement('div');  //creamos un nuevo div 
    div.classList.add('mt-10'); //leagregamos clases
//usamos innerHTML para modificar el HTML y agregar contenido 
    div.innerHTML = ` 
        <p class="header">Tu Resumen</p> 
        <p class="font-bold">Marca: <span class="font-normal">${textoMarca}</span></p> 
        <p class="font-bold">Año: <span class="font-normal">${year}</span></p> 
        <p class="font-bold">Tipo: <span class="font-normal capitalize">${tipo}</span></p> 
        <p class="font-bold">Total: <span class="font-normal">$ ${total}</span></p>

    `;

    const resultadoDiv = document.querySelector('#resultado');//seleccionamos el id de resultado 

    //MOSTRAR EL SPINNER
    const spinner = document.querySelector('#cargando');
    spinner.style.display = 'block';

    setTimeout(() => {
        spinner.style.display = 'none'; //se borra el spinner pero
        resultadoDiv.appendChild(div) //se muestra el resultado 
    }, 3000);
}

//INSTANCIAR UI
const ui = new UI(); //aparece en el proto llenarOpciones

document.addEventListener('DOMContentLoaded', () => {
    ui.llenarOpciones(); //llenar el select con los años
})

eventListeners(); //manda allamar la funcion para validar el formulario 
function eventListeners(){
    const formulario = document.querySelector('#cotizar-seguro'); //selecciona y valioda el id de cotizar-seguro
    formulario.addEventListener('submit', cotizarSeguro); //escucha por el evento de submit 
}

function cotizarSeguro(e) {
    e.preventDefault()

    //leer la maraca seleccionada 
    const marca = document.querySelector('#marca').value;

    //leer  el año seleccionado 
    const year = document.querySelector('#year').value; //leemos el valor de value 
    //leer el tipo de cobertura 
    const tipo = document.querySelector('input[name="tipo"]:checked').value; //selecciona de manera especifica el name del input, checamos y despues leemos el valor de vlaue
    
    if(marca === '' || year ==='' || tipo === '') {
        ui.mostrarMensaje('Todos los campos son obligatorios', 'error');
        return; //hace que no se ejecute el siguiente mensaje solo si se lleno correctamente 
    } 
    ui.mostrarMensaje('Cotizando...', 'exito');

    //OCULTA LAS COTIZACIONES PREVIAS hace que los resltados no se repitan 
    const resultados = document.querySelector('#resultado div'); //seleciona el div del id de resultado 
    if(resultados != null) { //si resultado no es igual a null eliminamos el resultado, si cotizamos otro automoivl ese resultgado se elimina y genra otro diferente
        resultados.remove();
    }

    //instaciar el seguro 
    const seguro = new Seguro(marca, year, tipo); 
    const total = seguro.cotizarSeguro();

    //Utilizar el prototype que va a cotizar
    ui.mostrarResultado(total, seguro);
}