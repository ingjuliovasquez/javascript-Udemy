export default class Notificacion {
    constructor({texto, tipo}) {
        this.texto = texto;
        this.tipo = tipo;
        this.mostrar();
    }

    mostrar() {
        //Crear la notificacion 
        const alerta = document.createElement('DIV') //creamos un div
        alerta.classList.add('text-center', 'w-full', 'p-3', 'text-white', 'my-5', 'alert', 'uppercase', 'font-bold', 'text-sm') //le agregamos clases

        //ELIMINAR ALERTAS DUPLICADAS 
        const alertaPrevia = document.querySelector('.alerta');
        alertaPrevia?.remove();

        //SI ES DE TIPO ERROR, AGREGA UNA CLASE de background red si es de topo exito se le agregara la clase de background-green
        this.tipo === 'error' ? alerta.classList.add('bg-red-500') : alerta.classList.add('bg-green-500')

        //MENSAJE DE ERROR
        alerta.textContent = this.texto  //crea el texto

        //insertar en el Dom
        formulario.parentElement.insertBefore(alerta, formulario) //insertamos el texto de alerta ntes del formulario

        //quitar despues de 3 segundos
        setTimeout(() => {
            alerta.remove();
        }, 3000)
    }
} 