navegacion = document.querySelector('.navegacion')

console.log(navegacion.firstElementChild); //selecciona el primer elemento

console.log(navegacion.lastElementChild); //selecciona el ultimo elemento
console.log(navegacion.childNodes) //los espacios en blanco son considerados elementos 
console.log(navegacion.children) //selecciona los enlaces en la navegacion 


/*console.log(navegacion.children[1].nodeName); //nombre del atributo
console.log(navegacion.children[1].nodeType) //1 */

const card = document.querySelector('.card');

/*card.children[1].children[1].textContent = 'Nuevo heading desde treversing the dom'

card.children[0].src = 'img/hacer3.jpg';
console.log(card.children[0]);  //CAMBIA LA IMAGEN*/


//Traversing the Hijo al padre
/*console.log(card.parentNode);

console.log(card.parentElement.parentElement.parentElement);  //actrua sobre el elemento padre*/

/*console.log(card);
console.log(card.nextElementSibling);
console.log(card.nextElementSibling.nextElementSibling); //VA AL SIGUIENTE  hermano es decir a la siguiernte clase que se llame igual*/

const ultimoCard = document.querySelector('.card:nth-child(4)');
console.log(ultimoCard.previousElementSibling); //tre el ultimo card 



//previousElementSibling  VA AL ELEMENTO PREVIO O ANTERIOR 
//nextElementSibling  VA AL ELEMENTO SIGUIENTE