const btnFlotante = document.querySelector('.btn-flotante');
const footer = document.querySelector('.footer');

btnFlotante.addEventListener('click', mostrarOcultarFooter);

function mostrarOcultarFooter() {
    if( footer.classList.contains('activo')) { //nos ayuda a saber si una clase esta o no esta
        footer.classList.remove('activo'); //aparece y desaparece la clase
        this.classList.remove('activo');
        this.textContent = 'Idioma y Moneda'
    } else {
        footer.classList.add('activo');
        this.classList.add('activo');
        this.textContent = 'X Cerrar';   
    }
} 
//cambia las clases y el texto del boton 