const enlace = document.createElement('a'); //indica que vamos a agregar un nuevo enlace

//AGREGANDOLE EL TEXTO 
enlace.textContent = 'Nuevo Enlace';

//AÑADIENDO href
enlace.href = '/nuevo-enlace';

console.log(enlace);
enlace.target = "_blank" //agrega un target
enlace.setAttribute('data-enlace', 'nuevo-enlace') //agrega un atributo
enlace.classList.add('alguna-clase') //agrega una nueva clase

//seleccionar la navegacion 
const navegacion = document.querySelector('.navegacion');
//console.log(navegacion.children); //NOS AYUDA A SABER CUAL ES LA POSICION DE LOS ELEMENTOS
navegacion.insertBefore(enlace, navegacion.children[1]); //determina que el nuevo enlace se va agregar antes de la posicion numero 1


//CREAR UN CARD 
const parrafo1 = document.createElement('p');
parrafo1.textContent = 'Concierto';
parrafo1.classList.add('categoria', 'concierto');

const parrafo2 = document.createElement('P');
parrafo2.textContent = 'Concierto de Rock';
parrafo2.classList.add('titulo');

const parrafo3 = document.createElement('P');
parrafo3.textContent = '$800 por persona';
parrafo3.classList.add('precio');


//CREAR DIV CON LA CLASE DE INFO 
const info = document.createElement('div');
info.classList.add('info');
info.appendChild(parrafo1);
info.appendChild(parrafo2);
info.appendChild(parrafo3); //se crea un nuevo div con la clse info y cuyo contenido son los 3 parrafos que creamos anteriormente 

/* 
<div class="info"><p class="categoria concierto">Concierto</p><p class="titulo">Concierto de Rock</p><p class="precio">$800 por persona</p></div>
*/

//  CREAR LA IMAGEN 
const imagen = document.createElement('img');
imagen.src = 'img/hacer2.jpg';
//crea una imagwn

//CREAR UN CARD
const card  = document.createElement('div');
card.classList.add('Card');

//ASIGNAR LA IMAGEN 
card.appendChild(imagen);

//ASIGNAR INFO
card.appendChild(info); //crea un div llamada card con un div hijo llamado info


/*<div class="Card"><img src="img/hacer2.jpg"><div class="info"><p class="categoria concierto">Concierto</p><p class="titulo">Concierto de Rock</p><p class="precio">$800 por persona</p></div></div> */

//INSERTAR EN EL HTML
const contenedor = document.querySelector('.hacer .contenedor-cards');
contenedor. insertBefore(card, contenedor.children[0]); //se inserta en html antes de la posicion 0