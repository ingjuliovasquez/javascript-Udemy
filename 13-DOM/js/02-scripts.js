//Seleccionar elementos por sus clases

const header = document.getElementsByClassName('header');
console.log(header) //selecciona la clse header

const hero = document.getElementsByClassName('hero');
console.log(hero) //selecciona la clase de hero

//Si las clases existen mas de 1 vez 
const contenedores = document.getElementsByClassName('contenedor')
console.log(contenedores)//seleciona todas las clases que se llamen contenedor

//Si una clase no existe 
const noExiste = document.getElementsByClassName('noExiste');
console.log(noExiste); //si no exite una clse aparecera como un arreglo vacio