//Event Bubbling 

const cardDiv = document.querySelector('.card');
const infoDiv = document.querySelector('.info');
const titulo = document.querySelector('.titulo');

cardDiv.addEventListener('click', e => {
    e.stopPropagation()
    console.log('Click en el card');
});

infoDiv.addEventListener('click', e => {
    e.stopPropagation()
    console.log('Click en info');
});

titulo.addEventListener('click', e => {
    e.stopPropagation()
    console.log('Click en el titulo');
});



// Event Bubling es cuando presionas en un evento, pero ese evento se propaga por muchos otros dando resultados inesperados
//stopPropagation evita que se propaguen 
