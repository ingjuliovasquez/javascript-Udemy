const producto = 'Monitor 20 pulgadas';

//.replace para remplazar letras 
console.log(producto);
console.log(producto.replace('pulgadas',  '""')); //remplaxza la palabra pulgadas por el signo de pulgadas

console.log(producto.replace('Monitor', 'Monitor Curvo')); //remplaza la palabra Monitor por monitor curvo

//.slice para cortar palabras 
console.log(producto.slice(0, 10) ); //corte de la letra 1 ala 10
console.log(producto.slice(2, 1)); //elimina todo el cotenido

//Alternativa a slice
console.log(producto.substring(0,10));
console.log(producto.substring(2, 1));