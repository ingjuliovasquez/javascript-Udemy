const producto = 'Monitor 20 Pulgadas';
console.log(producto.toUpperCase()); //convierte todo a mayusculas

//toLowerCase
console.log(producto.toLowerCase()); //convierte todo a minusculas

const email = "CORREO@CORREO.COM";

console.log(email.toLowerCase());

const precio = 300;
console.log(precio);
console.log(precio.toString());