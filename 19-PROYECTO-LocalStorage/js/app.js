/*                VARIABLES               */
const formulario = document.querySelector('#formulario'); //seleccionamos el id de formulario 
const listaTweets = document.querySelector('#lista-tweets'); //seleccionamos el id de lista-tweets
let tweets = [];  //es una variable madificable



/*        EVENTS LISTENERS             */
eventListeners();

function eventListeners() {
    //Cuando el usuario agrega un nuevo tweet
    formulario.addEventListener('submit', agregarTweet); //escucghamos por el evenyo submit y la funcion agregar Tweet

    //Cuando el documento esta listo
    document.addEventListener('DOMContentLoaded', () => {
        tweets= JSON.parse(localStorage.getItem('tweets')) || []; 8//busca un tweet en localStorage y conviertelos a json.pasce pero si no hay que se se mantaenga como un arreglo vacio
        console.log(tweets);

        crearHTML();
    })


}



/*            FUNCIONES              */
function agregarTweet(e) {
    e.preventDefault();

    //Textarea donde el usuario escribe 
    const tweet = document.querySelector('#tweet'). value; //valida

    //validacion...
    if(tweet === '') { //si tweet es igual estrictamente a un string vacio 
        mostrarError('No puede ir vacio'); //mostrara un error 

        return; //evita que se ejecuten mas lineas de codigo
    }
    const tweetObj = {
        id: Date.now(), //nos sirve como un identificador unico basicamente son los segundos que han pasado desde 1970
        tweet
        // tweet: tweet solo cuando se llaman igual se pueden simplificar dejandolos con un solo nombre 
    }

    //Añadir al arreglo de tweets 
    tweets = [...tweets, tweetObj] //arreglo de objetos, copia la variable vacia y le agrega el tweet con id unico   
    crearHTML();

    //reiniciar el formulario
    formulario.reset();
}

 //Mostrar Mensaje de error 
 function mostrarError(error) {
    const mensajeError = document.createElement('p');
    mensajeError.textContent = error;
    mensajeError.classList.add('error') //agregamos la clase llamada error

    //Insertarlo en el html
    const contenido = document.querySelector('#contenido'); //agregamos el id de contenido 
    contenido.appendChild(mensajeError); //se agrega al final del formulario 

    //Elimina el mensaje de error despues de 3 segundos 
    setTimeout(() => {
        mensajeError.remove()
    }, 3000);
}

//Muestra un  listado de los tweets
function crearHTML() {

    limpiarHTML(); //primero se limpia y despues se agrega al html

    if(tweets.length > 0) { //si la variable tweets en mayor a 0
        tweets.forEach( tweet => { //tweets creara un nuevo arreglo 

            //Agregar un boton de eliminar 
            const btnEliminar = document.createElement('a'); //crea un enlace que funcionara como boton
            btnEliminar.classList.add('borrar-tweet');//agrega una clase de la hoja de estilo
            btnEliminar.innerText = 'X'; //inserta al html

            //Añadir una funcion de eliminar 
            btnEliminar.onclick = () => {
                borrarTweet(tweet.id); //Seleccionamos el id unico de cada tweet
            }

            //Crear el HTML
            const li = document.createElement('li'); //creara un listado de los tweets agregados al textarea

            //Añadir el texto 
            li.innerHTML = tweet.tweet; 

            //Asignar el boton 
            li.appendChild(btnEliminar);

            //Insertarlo en el HTML
            listaTweets.appendChild(li);
        })
    }

    sincronizarStorage();
}

//Agrega los tweets actuales a Localstorage
function sincronizarStorage() { //aunque salgamos de la pagina se quedaran almacenados los tweets en el localStorage 
    localStorage.setItem('tweets', JSON.stringify(tweets)) //los convierte a string 
}

//Elimina un tweet
function borrarTweet(id) {
    tweets = tweets.filter(tweets => tweets.id !== id); //filtra todos exepto el que estamos selecionando 

    crearHTML();
}

function limpiarHTML() { //mientras haya elementos se eliminara el primer hijo para evitar que se repitan 
    while(listaTweets.firstChild) {
        listaTweets.removeChild(listaTweets.firstChild)
    }
}