import {Cliente} from './cliente.js'; //importamos la clase de cliente


export class Empresa extends Cliente { //indica que empresa es un hijo de la clase padre cliente 
    constructor(nombre, ahorro, categoria) {
        super(nombre, ahorro); //hereda nombre y ahorro de cliente
        this.categoria = categoria; 
    }
    mostrarInformacion() { 
        return `Cliente: ${this.nombre} - Ahorro ${this.ahorro} - Categoria: ${this.categoria}`;
    }
}