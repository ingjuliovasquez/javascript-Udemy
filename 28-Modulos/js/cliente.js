//cuando tenemos varios archivos para un solo proyecto pudiera pasar que haya variables que se llamen igual y por asi decirlo se convinen los archivos, eso se puede eviar añadiendo un paretesis al principio y el final de la funcion 

//IIFE significa expresion de funcion ejecutada inmediatamente  eso mantiene las funciones en un solo archivo haciendo que no se mezclen en otros
/*(function() {
    console.log('Desde un IIFE');

    windownombreCliente = 'Juan' //añade a la ventana globla sin importar el archivo 
})();*/ 

//hay una mejor manera 

//export saca una variable, funcion, clase u objeto  de un archivo 
export const nombreCliente = 'Juan';
export const ahorro = 200;

//exportar una funcion  
export function mostrarInformacion( nombre, ahorro ) {
    return `Cliente: ${nombre} - Ahorro ${ahorro}`
}

export function tieneSaldo(ahorro) {
    if(ahorro > 0) {
        console.log('Si tiene saldo')
    } else {
        console.log('El cliente no tiene saldo')
    }
}

//exportar una funcion 
export class Cliente {
    constructor(nombre, ahorro) {
        this.nombre = nombre;
        this.ahorro = ahorro;
    }

    //agregamos un metodo denro de la clase
    mostrarInformacion() {
        return `Cliente: ${this.nombre} - Ahorro ${this.ahorro}`
    }
}

//solo se puede tener un export default
export default function nuevaFuncion() {
    console.log('Este es el export por default')
}