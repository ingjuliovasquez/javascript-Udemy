//import nos permite traerlo hacia este archivo  

//trae el archivo de la misma carpeta que se extrajo
import nuevaFuncion, { nombreCliente as clientenombre, ahorro, mostrarInformacion, tieneSaldo, Cliente } from "./cliente.js";  //se pone una coma para evitar escribir lo mismo dos veces
//la funcion por default NO debe estar dentro de las llaves 
//se le puede poner un alias a la funcion con un as

import {Empresa} from './empresa.js'; //se recomikenda que los import siempre esten en la parte de arriba

nuevaFuncion(); //madamos llamar la funcion 

console.log(clientenombre)

/*console.log(nombreCliente);
console.log(ahorro);

console.log(mostrarInformacion(nombreCliente, ahorro));
 //llamar una funcion
tieneSaldo(ahorro);

//llamar una clase 
const cliente = new Cliente(nombreCliente, ahorro); // el ahorro de juan

console.log(cliente.mostrarInformacion())

//importar empresa
const empresa = new Empresa('Codigo con Juan', 100, 'Aprendizaje en Linea');
console.log(empresa.mostrarInformacion());*/