const numero1 = 20;
const numero2 = "20";
const numero3 = 30;

//Revisar si 2 numeros son iguales
console.log(numero1 == numero3); //false
console.log(numero1 == numero2); //Pone true porque no diferencia si es um string o un numero normal

//Comparador estricto 
console.log(numero1 === numero2); //POne falso porque si diferencia entre un string y un numero normal
console.log(numero1 === parseInt(numero2)); //primero se convirtio a string y luego se comparo
console.log(typeof numero1); //determina que es de tipo numero 
console.log(typeof numero2); //determina que es de tipo string


//Diferente
const password1 = "admin";
const password2 = "Admin";

console.log(password1 != password2); //indica que ambas contraseñas si son diferentes 
console.log(numero1 != numero2); //incaca que son iguales aunque uno sea string pero de am
console.log(numero1 !==parseInt(numero2));