(function() {
    let DB; //se mantiene de forma global 
    let idCliente;

    const nombreInput = document.querySelector('#nombre');
    const emailInput = document.querySelector('#email');
    const telefonoInput = document.querySelector('#telefono');
    const empresaInput = document.querySelector('#empresa');
    
    const formulario = document.querySelector('#formulario')

    document.addEventListener('DOMContentLoaded', () => {
        conectarDB();  //conecta la base de datos 

        formulario.addEventListener('submit', actualizarCliente);

        const parametrosURL = new URLSearchParams(window.location.search); //busca los parametros disponibles en la url, cuando el documento esta listo el id se lee directamente del url

        const idCliente = parametrosURL.get('id'); //obtenemos los id del querystring 
        if(idCliente) { //si tenemos el id
            setTimeout(() => { //DB tarda en conectarse con la base de datos, ese tiempo puede retrasarse utilizando un setTimeout
                obtenerCliente(idCliente);
            }, 100);  
        }
    });

    function actualizarCliente() {
        e.preventdefault();

        if(nombreInput.value === '' || emailInput.value === '' || empresaInput.value === '' || telefonoInput.value === '');
        imprimirAlerta('Todos los campos son obligatorios', 'error');

        //ACTUALIZAR CLIENTE
        const clienteActualizado = {
            nombre: nombreInput.value,
            email: emailInput.value,
            empresa: empresaInput.value,
            telefono: telefonoInput.value,
            id: Number(idCliente) //concierte el string a un numero 
        }

        const transaction = DB.transaction(['crm'], 'readwrite');
        const objectStore = transaction.objectStore('crm');

        objectStore.put(clienteActualizado);

        transaction.oncomplete = function() {
            imprimirAlerta('Editado correctamente');

            setTimeout(() => {
                window.location.href = 'index.html'
            }, 3000);
        }
        transaction.onerror = function(){
            imprimirAlerta('Hubo un error', 'error');
        }
    }

    function obtenerCliente(id) { //toma el id 
        const transaction = DB.transaction(['crm'], 'readonly');
        const objectStore = transaction.objectStore('crm');

        const cliente = objectStore.openCursor(); 
        cliente.onsuccess = function(e) { //en caso de que se haya obtenido correctamente
            const cursor = e.target.result; 

            if(cursor) {
                if(cursor.value.id === Number(id)) { //si el id del url es igual al id del cursor 
                    llenarformulario(cursor.value);
                }
                cursor.continue();
            }
        }
    }

    function llenarformulario(datoscliente) { //hace que se llene el formulario con los datos que queremos editar
        const {nombre, email, telefono, empresa} = datoscliente; //destructuring

        nombreInput.value = nombre; 
        emailInput.value = email; 
        telefonoInput.value = telefono; 
        empresaInput.value = empresa; 
    }
})();