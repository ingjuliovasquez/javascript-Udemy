(function() {
    let DB; 

    const formulario = document.querySelector('#formulario'); //seleccionamos el id e formulario z

    document.addEventListener('DOMContentLoaded', () => {
        conectarDB(); //cuando el doumento este listo se ejecuta la funcion 
        
        formulario.addEventListener('submit', validarCliente); //escuchar por el evento submit y ejecuta la funcion validar formulario
        
    }); 

    function validarCliente(e) {
        e.preventDefault();

        //leer los datos el imput del id con value
        //input es el lugar en donde escribimos la informacion para llenar un formulario 
        const nombre = document.querySelector('#nombre').value;
        const email = document.querySelector('#email').value;
        const telefono = document.querySelector('#telefono').value;
        const empresa = document.querySelector('#empresa').value;

        if(nombre === '' || email === '' || telefono === '' || empresa === '') { //en caso de que alguno de los campos este vacio aparecera un mensaje de alerta 
            imprimirAlerta('Todos los campos son obligatorios', 'error');

            return;
        }
        //crear un objeto con la informacion uilizando OBJECT LITERAL es lo contrario de DESTRUCTURING
        const cliente = {
            nombre,
            email, 
            telefono, 
            empresa
        }
        cliente.id = Date.now(); //agregamos date a el id de cliente para que sea unico 

        crearNuevoCliente(cliente);
    }

    function crearNuevoCliente(cliente) {

        //agregar a al abase de datos un Nuevo cliente
        const transaction = DB.transaction(['crm'], 'readwrite'); //conectamos n uestra base de datos
        const objectStore = transaction.objectStore('crm');

        objectStore.add(cliente);

        transaction.onerror = () => {
            console.log('Hubo un error');
        }

        transaction.oncomplete = () => {
            console.log('Cliente Agregado');

            imprimirAlerta('El cliente fue agregado correctamente');

            setTimeout(() => {
                window.location.href = 'index.html';
            }, 3000);
        }

        DB = abrirConexion.result;
    }
})();