(function(){ // Es un IIFE se ejecuta tan pronto como se define

    let DB; //creamos la variable

    document.addEventListener('DOMContentLoaded', () => {
        crearDB(); //cuando el documento este listo se ejecutara la funcion
        if(window.indexedDB.open('crm', 1)) {
            obtenerClientes();
        }
    });

    //CREAR LA BASE DE DATOS de indexedDB
    function crearDB(){
        const crearDB = window.indexedDB.open('crm', 1); //obtenemos acceso a indexedDB

        crearDB.onerror = function() {
            console.log('HUbo un error');
        };

        crearDB.onsuccess = function() {
            DB = crearDB.result; //el resultado se asigna a la variable de DB
        };

        crearDB.onupgradeneeded = function(e) { //solo se ejecuta una vez y hace que cuando se ejecute el codigo se creen las columnas 
            const db = e.target.result

            const objectStore = db.createObjectStore('crm', { keyPath: 'id', autoIncrement: true}); //toma como resferencia el id para que sea unico

            objectStore.createIndex('nombre', 'nombre', {unique: false});
            objectStore.createIndex('email', 'email', {unique: true});
            objectStore.createIndex('telefono', 'telefono', {unique: false});
            objectStore.createIndex('empresa', 'empresa', {unique: false});
            objectStore.createIndex('id', 'id', {unique: true});

            console.log('DB creada')
        }
    }

    function obtenerClientes() {
        const abrirConexion = window.indexedDB.open('crm', 1);

        abrirConexion.onerror = function() {
            console.log('Hubo un error')
        };
        abrirConexion.onsuccess = function() {
            DB = abrirConexion.result;

            const objectStore = DB.transaction('crm').objectStore('crm');

            objectStore.openCursor().onsuccess = function(e) { //cursor se coloca en la posicion 0 del crm de la base de datos y va a leyendo los resultados del siguiente, siguiente, etc
                const cursor = e.target.result;

                if(cursor) {
                    const { nombre, empresa, email, telefono, id } = cursor.value;

                    const listadoClientes = document.querySelector('#listado-clientes');
                    listadoClientes.innerHTML += `

                        <tr>
                            <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200">
                                <p class="text-sm leading-5 font-medium text-gray-700 text-lg  font-bold"> ${nombre} </p>
                                <p class="text-sm leading-10 text-gray-700"> ${email} </p>
                            </td>
                            <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 ">
                                <p class="text-gray-700">${telefono}</p>
                            </td>
                            <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200  leading-5 text-gray-700">    
                                <p class="text-gray-600">${empresa}</p>
                            </td>
                            <td class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5">
                                <a href="editar-cliente.html?id=${id}" class="text-teal-600 hover:text-teal-900 mr-5">Editar</a>
                                <a href="#" data-cliente="${id}" class="text-red-600 hover:text-red-900">Eliminar</a>
                            </td>
                        </tr>
                    `;

                    cursor.continue();

                } else {
                    console.log('No hay mas registros...');
                }
            }
        }
    }
})();