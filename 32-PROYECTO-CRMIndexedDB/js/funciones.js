//Un archivo de funciones evita la repeticion de estas en diferentes archivos
function conectarDB() {
    let abrirConexion = window.indexedDB.open('crm', 1); //nos permite crear una base de datos con la versoion 1

    abrirConexion.onerror = function() { //en caso de que no se pueda agregar a la base de datos se notificara un error 
        imprimirAlerta('Hubo un error', 'error')
    }

    abrirConexion.onsuccess = function() { //si se agregado correctamente el resultaro se agregara al DB
        DB = abrirConexion.result;
    }
}

function imprimirAlerta(mensaje, tipo) { //se le garegan dos parametros el mensaje que queremos que aparezca y el tipo esto nos ayudara a por asi decirlo generar un codigo que pueda ocuparse con distintos resultados pero utilizando la misma formula 

    const alerta = document.querySelector('.alerta'); //seleccionamos la clase de alerta del docuento de css

        if(!alerta) { //si no hay una alerta previa apercera, esto evita que la arlerta aparezca multiples veces 

        //crear alerta 
        const divMensaje = document.createElement('div'); //creamos un div para ejcutar el mensaje
        divMensaje.classList.add('px-4', 'py-3', 'rounded', 'max-w-lg', 'mx-auto', 'mt-6'), 'text-center', 'border'; //esilos del mesjae principal

        if(tipo === 'error') { //definios que solo se ejecutara el tipo error cuando los campos esten vaciosde no ser asi se ejecutara el mensaje en color verde 
            divMensaje.classList.add('bg-red-100', 'border-red-400', 'text-red-700'); //estilos del mesaje de error (rojo)
        } else {
            divMensaje.classList.add('bg-green-100', 'border-green-400', 'text-green-700'); //estilos del mensaje de correcto (verde)
        }

        divMensaje.textContent = mensaje; //utilixamos el mensaje 'Todos los campos son obligatorios'

        formulario.appendChild(divMensaje); //agregamos el mensaje en la pantalla al final del formulario 

        setTimeout(() => { 
            divMensaje.remove();
        }, 3000); //el mensaje solo aparcera en la pantalla 3 segundos 
    }

}