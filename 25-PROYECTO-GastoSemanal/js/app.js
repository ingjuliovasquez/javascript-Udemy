/*       VARAIABLES Y SELECTORES     */ 
const formulario = document.querySelector('#agregar-gasto'); //se selecciona el id de agregar-gastos
const gastoListado = document.querySelector('#gastos ul') //se selecciona el id de gastos y su hijo ul
/*         EVENTOS                   */
eventListeners(); //se lama la funcion
function eventListeners() {
    document.addEventListener('DOMContentLoaded', preguntarPresupuesto); //en el momento que el documento esta liksto preguntara cual es el presupuestp
    formulario.addEventListener('submit', agregarGastos)
}
/*           CLASES                  */
class Presupuesto {
    constructor(presupuesto) { //crea una instancia de una clase
        this.presupuesto = Number(presupuesto) //Number trabaja con numeros enteros y decimales tambien se asgura que se escriban solo numeros y no letras
        this.restante = Number(presupuesto); //nos dice el dinero que aun tenemos disponible
        this.gastos = [] //comienza vacioo para que el usuario vaya agregandolo despues 
    }
    nuevoGasto( gasto ) {
        this.gastos = [...this.gastos, gasto]; //copiamos y le agregamos las propiedades de gasto
        this.calcularRestante();
    }

    calcularRestante() {
        const gastado = this.gastos.reduce((total, gasto) => total += gasto.cantidad, 0 ); //reduce utiliza dos valores el total de lo que tenemos y lo que le queremos restar, es esta funcion vamsos sumando lo que hemos gastado para despues restarlo al valor inicial 
        this.restante = this.presupuesto - gastado; //le retamos lo gastado al presupuesto inicial
    }

    eliminarGasto(id) {
        this.gastos = this.gastos.filter( gasto => gasto.id !== id ); //iterar sobre cada gasto y vamos a filtrar o a traernos todos excepto el id que estamos tratando de eliminar
        this.calcularRestante();
    }
}

class UI {
    insertarPresupuesto(cantidad) {
        //EXTRAYENDO LOS VALORES 
        const {presupuesto, restante}   = cantidad;

        //AGREGAMOS AL HTML
        document.querySelector('#total').textContent = presupuesto; //selecionamos el id el presupuesto que seleccionamos en la ventana de windows aparezca en el html
        document.querySelector('#restante').textContent = restante;
    }

    imprimirAlerta(mensaje, tipo) {
        //CREAR EL DIV
        const divMensaje = document.createElement('div'); //creamos un div
        divMensaje.classList.add('text-center', 'alert'); //agregamos clases de estilos

        if(tipo === 'error') { //si tipo es igual a error arrojara una alerta en color rojo
            divMensaje.classList.add('alert-danger');
        } else {
            divMensaje.classList.add('alert-success') //de lo contario sera una alerta de color verde indicando que es correcto
        }

        //MENSAJE DE ERROR 
        divMensaje.textContent = mensaje;

        //INSERTAR EN EL HTML
        document.querySelector('.primario').insertBefore(divMensaje, formulario); //insertamos en mensaje antes del formulario 

        //quitar el alert despues de 3 segundos
        setTimeout(() => {
            divMensaje.remove();
        }, 3000);
    }

    mostrarGastos(gastos) {

        this.limpiarHTML();

        //iterar sobre los gastos 
        gastos.forEach(gasto => {
            const {cantidad, nombre, id}  = gasto; //destructuring

            //crear un li
            const nuevoGasto = document.createElement('li');
            nuevoGasto.className = 'list-group-item d-flex justify-content-between aling-items-center'//agrega las clases del css
            nuevoGasto.dataset.id = id; //es igual a data-id 

            //Agregar el HTML del gasto 
            nuevoGasto.innerHTML = `
                ${nombre}
                <span class="badge badge-primary badge-pill ">$ ${cantidad}</span>
            `; //crea lo que se agregara en el html

            //Boton paar borra el gasto 
            const btnBorrar = document.createElement('button'); //crea un boton 
            btnBorrar.classList.add('btn', 'btn-danger', 'borrar-gasto'); //clases css de boton
            btnBorrar.innerHTML = 'Borrar &times' //&times hace que el texto del boton borrar tenga una x a un lado
            btnBorrar.onclick = () => { //esta sintaxis nos permite ejecuta la funcion solamente cuando se le de click al boton de borrar
                eliminarGasto(id); //al ser un id unico nos permite saber que elemento estamos borrando
            }

            nuevoGasto.appendChild(btnBorrar);//lo agrega al final

            //AGREGAR EN EL HTML
            gastoListado.appendChild(nuevoGasto);
    
        });
    }

    limpiarHTML() { //si tiene algo se eliminara el primer hijo eviatndo que los valores agregados se repitan en el html
        while(gastoListado.firstChild) {
            gastoListado.removeChild(gastoListado.firstChild)
        }
    }

    actualizarRestante(restante) {
        document.querySelector('#restante').textContent = restante; //restamos el restante del presupuesto
    }

    comprobarPresupuesto(presupuestoObj) {
        const {presupuesto, restante} = presupuestoObj; //destructuring

        const restanteDiv = document.querySelector('.restante');

        //COMPROBAR 25%
        if(( presupuesto / 4) > restante) { //si el presupuesto dividivo entre 4, es decir, el 25% es mayor que restante
            restanteDiv.classList.remove('alert-success', 'alert-warning'); //eliminara la clase alert-success y alerta-warning haciend que en algunos gastos se pase directamente a alert-danger
            restanteDiv.classList.add('alert-danger'); //agrega alert-danger
        } else if( ( presupuesto / 2) > restante) { //si la mitad del presupuesto es mayor que el restante eliminara y agregara las siguientes clases
            restanteDiv.classList.remove('alert-success'); 
            restanteDiv.classList.add('alert-warning');
        } else { //se ejecuta cuando se hace un reembolsa de los gastos que eliminamos del html 
            restanteDiv.classList.remove('alert-danger', 'alert-warning');
            restanteDiv.classList.add('alert-success')
        }

        //SI EL TOTAL ES 0 O MENOR 
        if( restante <= 0) {
            ui.imprimirAlerta('El presupuesto se ha agotado', 'error') //imprime una alerta cuando ya sobrepasamos el valor de restante

            //DESHABILITAR EL BOTON DE AGREGAR CUANDO YA SE HAYA ACABADO EL PRESUPUESTO
            formulario.querySelector('button[type="submit"]').disabled = true; 
        }
        
    }
}

//INSTANCIAR 
const ui = new UI; //se mantiene global

let presupuesto; //creamos una variable vacia para que sea una variable global y podamos instanciar en cualquier funcion
/*           FUNCIONES               */

function preguntarPresupuesto() {
    const presupuestoUsuario = prompt('¿Cual es tu presupuesto?');  //promptes un metodo del objeto window que muestra un cuadro de dialogo con un mensaje que solicita al usuario, es parecido al alert 

    if(presupuestoUsuario === '' || presupuestoUsuario === null || isNaN(presupuestoUsuario) || presupuestoUsuario <= 0) { //si presupuestoUsuario es un string vacio o sea null recargara la pagina
    //si presupuesto usuario es igual a isNan va a retornar un true
    //es caso de que el presupuesto usuario sea menor o igual a 0 le volvera a preguntar
        window.location.reload(); //loaded recarga la vetana actual en c que no hayan contestado la pagina se vuelve a recargar 
    }

    //PRESUPUESTO VALIDO 
    presupuesto = new Presupuesto(presupuestoUsuario);
    console.log(presupuesto); 

    ui.insertarPresupuesto(presupuesto)
}

function agregarGastos(e) { //le ponemos epara que el evento de tipo submit pueda ejecutarse 
    e.preventDefault(); //preventiene la accion por default del submit

    //LEER LOS DATOS DEL FORMULARIO 
    const nombre = document.querySelector('#gasto').value //value ees un a forma directa de accerder al valor del imputo, nos da a conocer lo que estamos escribiendo sobre el 
    const cantidad = Number(document.querySelector('#cantidad').value); //number convierte lols numeros que se escriban en la cntidad de string a numeros normales

    //VALIDAR 
    if(nombre === '' || cantidad === '') { //comprueba que los campos no esten vacios
        //2 parametros: mensaje y tipo
        ui.imprimirAlerta('Ambos campos son obligatorios', 'error');
        
        return; //evitga que se ejecute el siguiente codigo en caso de que esa condicion ya se alla cumplido

    } else if(cantidad <= 0 || isNaN(cantidad)) { //si cantidad es igual a 0 o se escribe algun valor no valido aparecera la alerta 
        ui.imprimirAlerta('Cantidad no valida', 'error')

        return;
    } else {
        
        //generar un objeto con el gasto
        const gasto = { nombre, cantidad, id: Date.now() } //crea un objeto con un id unico, es tambien un arreglo de objetos 

        //AÑADE UN NUEVO GASTO
        presupuesto.nuevoGasto( gasto );

        //mensaje de todo bien 
        ui.imprimirAlerta( 'Gasto agregado correctamente', 'correcto')

        //imprimir los gastos 
        const { gastos, restante } = presupuesto; //aplicamos destructuring para estraer solo los gastos de presupuesto 
        ui.mostrarGastos(gastos);

        ui.actualizarRestante(restante);

        ui.comprobarPresupuesto(presupuesto);

        //reinicia formulario 
        formulario.reset();
    }
}

function eliminarGasto(id){
    //ELIMINA DEL OBJETO
    presupuesto.eliminarGasto(id);

    //ELIMINAR LOS GASTOS DEL HTML
    const {gastos, restante} = presupuesto; //extraemos gastos de presupuesto para poder pasarlo al metodo
    ui.mostrarGastos(gastos);

    ui.actualizarRestante(restante);

    ui.comprobarPresupuesto(presupuesto);
}