/*for(let i = 0; i <= 10; i++) { //ENLISTA LOS NUMEROS DEL 0 AL 10
    console.log(`Numero: ${i}` )
}*/

/*for( let i = 1; i <= 20; i++) { // i SIGNIFICA INDICE 
    // i++ SUMA LAS COANTIDADES 
    if( i % 2 === 0) {
        console.log(`El numero ${i} es PAR`);
    } else {
        console.log(`El numero ${i} es IMPAR`)
    } //ENLISTA NUEMRO PAR E IMPAR
}*/

//ESTRUCTURA DE UN CARRITO
const carrito = [
    { nombre: 'Monitor 27 pulgadas', precio: 500},
    { nombre: 'Television', precio: 100},
    { nombre: 'Tablet', precio: 200},
    { nombre: 'Audifonos', precio: 300},
    { nombre: 'Teclado', precio: 400},
    { nombre: 'Celular', precio: 700},
]
console.log( carrito.length) //nos dice cuantos elementos hay en el carrito

for( let i = 0; i < carrito.length; i++) { //nos permite saber cuando se agrega o se elimina un articulo del carrito
    console.log(carrito[i].nombre)
}
