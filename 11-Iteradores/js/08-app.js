const automovil = {
    modelo: 'Camaro',
    year: '1969',
    motor: 6.0
}

/*for(let propiedad in automovil) {
    console.log(`${automovil[propiedad]}`)
}*/

for( let [llave, valor] of Object.entries(automovil)) {
    console.log(valor);
    console.log(llave);
} //esta es una forma mas sencilla de acceder a las laves a los valores 

//valores camaro, 1969, 6.0
//llaves modelo: year: motor: 

