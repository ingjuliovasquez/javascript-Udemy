// El while loop, se ejecuta mientras una condición sea verdadera..

let i = 1;
while (i < 20) { // condicion  tambien funciona como un for
    if (i % 2 === 0) {
        console.log(`El ${i} es un par`)
    } else {
        console.log(`El ${i} no es un par`)
    }

    i++; // incremento
}

// derencia entre si es o no es un par