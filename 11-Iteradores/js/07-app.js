const pendientes = ['Tarea', 'Comer', 'Proyecto', 'EStudiar JavaScript']; //arreglo tradicional

const carrito = [ //areglo de objetos
    { nombre: 'Monitor 27 pulgadas', precio: 500},
    { nombre: 'Television', precio: 100},
    { nombre: 'Tablet', precio: 200}, 
    { nombre: 'Audifonos', precio: 300},
    { nombre: 'Teclado', precio: 400},
    { nombre: 'Celular', precio: 700},
]

//es una sintaxis de forEach y for
for( let pendiente of pendientes ) {
    console.log(pendiente);
}

for( let producto of carrito ) {
    console.log(producto.nombre) //accede al nombre de los articulos
}