/*for(let i = 0; i <= 10; i++) { 
    if(i === 5) {
        console.log('Este es el 5')
        break; //break detiene el for loop aunque no se alla cumplido la condicion, en este caso llegar al numero 10
    }
    console.log(`Numero: ${i}`);
}*/


/*for(let i = 0; i <= 10; i++) { 
    if(i === 5) {
        console.log('cinco')
        continue; //no detiene la ejecucion de for loop en este caso marca con letra el numero 5 y continua ejecutando los demas numeros 
    }
    console.log(`Numero: ${i}`);
}*/


const carrito = [
    { nombre: 'Monitor 27 pulgadas', precio: 500},
    { nombre: 'Television', precio: 100},
    { nombre: 'Tablet', precio: 200, descuento: true}, 
    { nombre: 'Audifonos', precio: 300},
    { nombre: 'Teclado', precio: 400},
    { nombre: 'Celular', precio: 700},
]
for(let i = 0; i < carrito.length; i++) {
    if(carrito[i].descuento) { //si ya sabemos que descuento es true no hace falta evaluarlo
        console.log(`El articulo ${carrito[i].nombre} Tiene descuento `);
        continue;
    } //arroja el nombre del articulo que tiene un descuento y comntinua enlistando los articulos disponibles del carrito
    console.log(carrito[i].nombre)
}