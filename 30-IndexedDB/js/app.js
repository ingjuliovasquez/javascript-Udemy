let DB;

document.addEventListener('DOMContentLoaded', () => { //cuando el documento este listo manda a llamar la funcion 
    crmDB();

    setTimeout(() => {
        crearCliente();
    }, 3000);
})

function crmDB() {
    //open permite crear base de datos version 1.0
    let crmDB = window.indexedDB.open('crm', 1.1); //el primer elemnto es un nombre en string y el segundo es la version que estamos utilizando 

    //en caso de que haya un error por ejemplo que el navegador no soporte indexedDB
    crmDB.onerror = function() { 
        console.log('Hubo un eroor a la hora de crear la base de datos') 
    }

    //en caso de que se cree correctamente
    crmDB.onsuccess = function() {
        console.log('Base de datos creada')

        DB = crmDB.result; //el resultado sera asignado al DB
    }

    //configuracion de la base de datos 
    crmDB.onupgradeneeded = function(e) { //cuando se ejecuta  onupgradeneeded se pasa automaticamnete un evento
        const db = e.target.result;
        
        const objectStore = db.createObjectStore('crm', { //creamos una base de dstos en el elemento de crm para poder crear las columnas 
            keyPath: 'crm', //
            autoIncrement: true //les asifna el id 
        })

        //Definir las columnas 
        objectStore.createIndex('nombre', 'nombre', {unique: false}); //valores unicos 
        objectStore.createIndex('email', 'email', {unique: false});
        objectStore.createIndex('telefono', 'telefono', {unique: false});

        console.log('columnas creadas')
    }
}

function crearCliente() {//la transaccion se }ra en la base de datos de crm
    let transaction = DB.transaction(['crm'], 'readwrite')  //el primer valor es donde queremos guardarlo y el segundo es el valor en este caso en read y write

    transaction.oncomplete = function() {
        console.log('Transaccion completada')
    }

    transaction.onerror = function() {
        console.log('Hubo un error en la transaccion')
    }

    const objectStore = transaction.objectStore('crm');

    const nuevoCliente = {
        telefono: 5714599,
        nombre: 'Juan',
        email: 'corereo@correo.com'
    }

    const peticion = objectStore.add(nuevoCliente) 
    //const peticion = objectStore.put(nuevoCliente) //nos permite actualizar 



    console.log(peticion);
}