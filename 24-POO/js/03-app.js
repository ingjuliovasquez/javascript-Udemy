class Cliente {  //las clases siempre deben empezar con mayuscula
    constructor(nombre, saldo) {
        this.nombre = nombre;
        this.saldo = saldo; 
    }

    mostrarInformacion() {
        return `Cliente: ${this.nombre}, tu saldo es de ${this.saldo}`
    } //en caso de que la funcion no se reescriba se pasara tal cual al sigueirtnte en caso de que se llame la funcion 

    static bienvenida() { //una propiedad estatica no requieren una instancia son unicas de la clase, se mandan a llamar directamente desde la clase
        return `Bienvenido al cajero`
    }
}

//HERENCIA
class Empresa extends Cliente { //empresa es una clase hijo de cliente
    constructor(nombre, saldo, telefono, categoria) {
        super(nombre, saldo); //super busca en la clase padre los atributos que se le pasen 
        this.telefono = telefono;
        this.categoria = categoria;
    }
    static bienvenia() { //reescribe el metodo e ignora el anterior
        return `Bienvenido al cajero de empresa`
    }

}
const juan = new Cliente('juan', 300);
const empresa = new Empresa('Codigo con juan', 500, 25444555, 'Aprendizaje en Linea')
console.log(empresa);
console.log(empresa.mostrarInformacion())

console.log(Cliente.bienvenida());
console.log(Empresa.bienvenida());

