class Cliente {  //class declaration
    constructor(nombre, saldo) {
        this.nombre = nombre;
        this.saldo = saldo; 
    }

    mostrarInformacion() {
        return `Cliente: ${this.nombre}, tu saldo es de ${this.nombre}`
    }

    static bienvenia() { //una propiedad estatica no requieren una instancia son unicas de la clase, se mandan a llamar directamente desde la clase
        return `Bienvenido al cajero`
    }
}
const juan = new Cliente('juan', 300);
console.log(juan.mostrarInformacion());
console.log(juan)

const Cliente2 = class { //class expression
    constructor(nombre, saldo) {
        this.nombre = nombre;
        this.saldo = saldo; 
    }

    mostrarInformacion() {
        return `Cliente: ${this.nombre}, tu saldo es de ${this.nombre}`
    }
}

const juan2 = new Cliente2('juan', 400)
console.log(juan2.mostrarInformacion());
console.log(juan2);

//ambos dan el mismo resultado pero es mas utilizada CLASS DECLARATION