class Cliente {  //las clases siempre deben empezar con 
    
    #nombre; //hace una propiedad privada solo se puede acceder o modificar los valores desde las clases

    setNombre(nombre) { //modifica
        this.#nombre = nombre;
    }
    getNombre() { //obtiene el valor
        return this.#nombre
    }

    
    /*constructor(nombre, saldo) { //son propiedades publicas
        this.nombre = nombre;
        this.saldo = saldo; 
    }

    mostrarInformacion() {
        return `Cliente: ${this.#nombre}, tu saldo es de ${this.saldo}`
    } //en caso de que la funcion no se reescriba se pasara tal cual al sigueirtnte en caso de que se llame la funcion 

    static bienvenida() { //una propiedad estatica no requieren una instancia son unicas de la clase, se mandan a llamar directamente desde la clase
        return `Bienvenido al cajero`
    }*/
}

const juan = new Cliente();
//console.log(juan.mostrarInformacion());//acedemos a la propiedad privada desde la clase
//console.log(juan.#nombre); //no se puede acceder de esta manera
juan.setNombre('Juan');
console.log(juan.getNombre());