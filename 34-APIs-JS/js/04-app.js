const abrirBtn = document.querySelector('#abrir-pantalla-completa')
const salirBtn = document.querySelector('#salir-pantalla-completa')

abrirBtn.addEventListener('click', pantallaCompleta)
salirBtn.addEventListener('click', cerrarPantallaCompleta)

function pantallaCompleta() {
    document.documentElement.requestFullscreen(); //nos permite acceder a la pantalla completa
}

function cerrarPantallaCompleta() {
    document.exitFullscreen()
}