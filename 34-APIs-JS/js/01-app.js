const notificarBtn = document.querySelector('#notificar');

notificarBtn.addEventListener('click', () => {
    Notification
        .requestPermission() //permite al usuario si quiere recibir notificaciones del la pagina, si el usuario acwepta se ejecutara la funcion de then 
        .then( resultado => {
            console.log('El resultado es', resultado);
        })
})

const verNotificacion = document.querySelector('#verNotificacion');
verNotificacion.addEventListener('click', () => {
    if(Notification.permission === 'granted') { //si el uisusario acepta las notificaciones 
        const notificacion = new Notification('Esta es la notificacion', {//aparecera que hemos recibido la notificacion que se puede personalizar
            icon: 'img/ccj.png', //se le puede agregar el icono de la pagina que notifica 
            body: 'Codificando' //mensaje de la notificacion 
        });

        notificacion.onclick = function() { //responde al evento de onclick y nos llevara directamente al sitio web del link
            window.open('https://www.codigoconjuan.com') //window.open abre una nueva ventana con el sitio
        }
    }
});