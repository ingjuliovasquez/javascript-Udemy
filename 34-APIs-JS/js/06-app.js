//es importante pedir permiso antes de empezar a utilizar la funcion de grabar la voz del usuario
const salida = document.querySelector('#salida');
const microfono = document.querySelector('#microfono');

microfono.addEventListener('click', ejecutarSpeeachAPI);

function ejecutarSpeeachAPI() {
    const SpeechRecognition = webkitSpeechRecognition;
    const recognition = new SpeechRecognition(); //creamos un nuevo objeto 

    //hay diferentes etapas del recorgnition

    recognition.start(); //arrancando 

    recognition.onstart = function() { //cuando recognition comienza a ejecutarse es decir escuchando (durante)
        salida.classList.add('mostrar') //añadimos la clase de mostrar
        salida.textContent = 'Escuchando...' //añadimos el texto escuchando
    }

    recognition.onspeechend = function() { //cuando se deja de ejecutar 
        salida.textContent = 'Se dejo de grabar...'
        recognition.stop(); //temina de escuchar 
    }

    recognition.onresult = function(e) {
        console.log(e.results[0][0]); //muestra el resultado 

        const {confidence, transcript} = e.results[0][0]; //destructuring
    
        const speech = document.createElement('p');
        speech.innerHTML = `Grabando: ${transcript}`;

        const seguridad = document.createElement('p');
        seguridad.innerHTML = `Seguridad: ${ parseInt( confidence * 100) } %`;

        salida.appendChild(speech);
        salida.appendChild(seguridad);
    }
}