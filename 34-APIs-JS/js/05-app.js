//es util para paausar o reproducir algun video cuando la ventana esta visible
document.addEventListener('visibilitychange', () => {
    if(document.visibilityState === 'visible') {
        console.log('Ejecutar la funcion de funcion para reproducir el video...')
    } else {
        console.log('Pausar el video')
    }
})