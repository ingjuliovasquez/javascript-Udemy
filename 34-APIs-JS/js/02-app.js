
document.addEventListener('DOMContentLoaded', () => { //cuando el documento este listo ejecutamos el observer
    const observer = new IntersectionObserver( entries => {  //permite identificar cuando un elemento entra o sale de la vista del usuario o del viewport( ventana visible del navegador) por ejemplo alguna imagen, cuando ya dimos scrool y ya no la vemos en la pagina 
        if(entries[0].isIntersecting) {
            console.log('Ya esta visible')
        }
    });

    observer.observe(document.querySelector('.premium')) //selecciona la clase de .premium 
})