const paises = [];

const nuevoPais = pais => new Promise( resolve => {
    setTimeout(() => {
        paises.push(pais);
        resolve(`Agregado: ${pais}`)
    }, 3000);
})

nuevoPais('Alemania')
    .then(resultado => { //es el resultado de resolve
        console.log(resultado);
        console.log(paises);
        return nuevoPais('Francia')
    })
    .then( resultado => { //cada 3 segundos se ire agregando un nuevo pais, este codigo es mas facil de leer que el ejemplo de callback del archivo  02-app.js
        console.log(resultado);
        console.log(paises);
        return nuevoPais('Ingleterra')
    })
    .then( resultado => {
        console.log(resultado);
    })