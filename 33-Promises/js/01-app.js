//callbacks 

const paises = ['Francia', 'España', 'Portugal', 'Australia', 'Inglaterra'] 

function nuevoPais(pais, callback) {
    setTimeout(() => {
        paises.push(pais); //actualiza
        callback();
    }, 2000); //despues de dos segundos agrega un nuevo pais 
}

function mostrarPaises() {
    setTimeout(() => {
        paises.forEach(pais => { //revisa cada uno de los elementos 
            console.log(pais);
        })
    }, 1000);
}

mostrarPaises();

nuevoPais('Alemania', mostrarPaises); //el callback es mostrarPaises