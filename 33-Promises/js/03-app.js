const aplicarDescuento = new Promise( (resolve, reject) => {
    const descuento = true;

    if(descuento) {
        resolve('Descuento Aplicado ')
    }else {
        reject('No sew pudo aplcae el descuento')
    }
})

aplicarDescuento //una vez que el promise se cumple then que significa entonces nos ayuda a seguir ejecutando la funcion
    .then( resultado => descuento(resultado)) //como es un arrow function se pueden eliminar las llaves y ademas se puede ejecutar una funcion una vez que se haya ejecutado el promise

    .catch(error => { //en caso de que no se cumpla el promise
        console.log(error);
    }) //funcion en casokde error

/* hay 3 valores posibles 
        fulfilled es decir qie el promise se cumplio
        rejected es decir que el promise no se cumplio
        pending signicfica que el promise no se ha cumplido pero tampoco se ha rechazado */