/*const autoenticado = true;
if(autoenticado === true) { //no es nececsario hacer una evaluacion de trye porque ya se da por echo por lo tanto el codigo puede sim´lificarse

    console.log('El usuario esta autoenticado')
} */ 


const autoenticado = true; 
if(autoenticado) {
    console.log('El usuario esta autoenticado')
}

const puntaje = 450;

function revisarPuntaje() { //return solo funciona dentro de una function
    if( puntaje > 400) { //es mejor ir de mayor a menor
        console.log('Excelente');
        return; //hace que si la primera condicion ya se cumplio las demas ya no se ejecuten
    }

    if(puntaje > 300) {
        console.log('Buen puntaje...  felicidades');
        return; 
    }
}
revisarPuntaje(); //manda a llamar una function