//Switch case

const metodoPago = 'efectivo';

switch(metodoPago) { //funciona como un if para comprobar variables
    case 'efectivo':
        pagar();
        break;
    case 'cheque':
        console.log(`Pagaste con ${metodoPago}`);
        break;
    case 'tarjeta':
        console.log(`Pagaste con ${metodoPago}`);
    default: //es muy similar a un else, funciona cuando ninguna de las consiciones se cumple
        console.log('Aún no has seleccionado un metodod de pago o metodo de pago no soportado')
        break;
}

function pagar() {
    console.log('Pagando...') //aparece en la consola pagando..
}