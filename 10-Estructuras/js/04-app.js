const dinero = 1000; //300
const totalAPagar = 300;
const tarjeta = true;
const cheque = true;

if(dinero >= totalAPagar) { 
    console.log('Si podemos pagar');
} else if (cheque) {
    console.log('Si tengo un cheque')
} else if(tarjeta) {
    console.log('Si puedo pagar porque tengo la tarjeta')
} else {
    console.log('Fondos insificientes');
}
//cuando los dos estan juntos ejecuta la primera condicion que se cumpla es decir ('Si podemos pagar')
//se ejecutan en cadena es decir si el saldo es insuficiente ejecutara ('Si tengo cheque')
//en el caso de que cheque sea false se ejecutara ('si puede pagar porque tengo tarjeta')
//en s¿caso que todos sea insuficiente se ejecutara ('fondos insuficientes')