//los generadores empiezan con un *

/*function *crearGenerador() {

    //son estaticos porque los definimos nosotros mismos 
    yield 1; //acepta numeros 
    yield 'Juan'; //string
    yield 3+3; //operaciones
    yield true; //booleans
}

const iterador = crearGenerador();

// next es una funcion del proto nos permiter iterar sobre el generor y es como si lo despertara
console.log(iterador) //nos lleva al iterador completo
console.log(iterador.next().value); //el valor del primer yield es 1
console.log(iterador.next()); //cada next se va al siguiente en este caso yield 'Juan';
console.log(iterador.next().value); //el valor del tercero es 6
console.log(iterador.next());//{value: true, done: false}
console.log(iterador.next()); 
console.log(iterador) //ubna vez que ya se recorrieron todos los yield el gnerador se vulve a cerrar*/

//generador para carrito de compras
function *generadorCarrito(carrito) {

    for( let i = 0; i < carrito.length; i++) { //se ejecuta mientras i sea menor al contenido del carrito 
        yield carrito [i]; //toma el valor de 0
    }
}

const carrito = ['Producto 1','Producto 2', 'Producto 3'];

const iterador = generadorCarrito(carrito)
console.log( iterador.next());
console.log( iterador.next());
console.log( iterador.next());
console.log( iterador.next()); //finalizo el generador 

