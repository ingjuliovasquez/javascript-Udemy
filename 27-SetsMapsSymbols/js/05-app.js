//Los Symbol no son iguales NUNCA 
//NO son iterables
const sym = Symbol();
const sym2 = Symbol();

/*if(sym === sym2) {
    console.log('Son iguales')
} else {
    console.log('Son diferentes') //imprime este codigo en la consola
}*/

/*console.log( Symbol() === Symbol());  es false porque nunca son iguales aunque se escriban diferentes*/

const nombre = Symbol();
const apellido = Symbol();

const persona = {} //objeto vacio

//Agregar nombre y apeolido como llaves del objeto
persona[nombre] = 'Madi'; //para acceder a los elementos del Symbol es necesario utilizar corchetes
persona[apellido] = 'Munguia';
persona.tipoCliente = 'Premium';
persona.saldo = 500

console.log(persona); //en la consola nombre y apellido aparecen como Symbol(): "Madi"

//console.log(persona[nombre]); //no se puede acceder a traves de la sintaxis de punto, solo con corchetes 

//las propiedades no son iterables
for(let i in persona) {
    console.log(i)
}

//Definir una descripcion del Symbol
const nombreCliente = Symbol('Nombre del Cliente');
const cliente = {}

cliente[nombreCliente] = 'Pedro';

console.log(cliente); //no da el symbol y la descripcion
console.log(cliente[nombreCliente]); //accedemos solo a pedro
console.log(nombreCliente); //solo nos da la descripcion del cliente