// MAPS son listas ordenadas en llave y valor, pueden ser cualquier tipo de dato. puede ser un arreglo o un numero, son diseñados para agregar, recorrer o eliminar elementos 

//sirve para agregar y quitar objetos

const cliente = new Map();

cliente.set('nombre', 'Karen');  //la llave es nombre y el valor es karen
cliente.set('tipo', 'Premiun');
cliente.set('saldo', 3000);


console.log(cliente.size) //nos permitge sabeer la extension 
console.log(cliente.has('nombre')); ///nos dice si un valor existe o no es definido con un true o con un false 

console.log(cliente.get('nombre')); //obtenemos un valor 
 
cliente.delete('saldo'); //eliminamos saldo 

console.log(cliente.has('saldo')); //comprobamos que salg¿do ya no existe 
console.log(cliente.get('saldo')); //no podemols obtener un valor porque saldo ya fue elimimado 

cliente.clear() //limpia o elimina todos los elementos 
console.log(cliente);

const paciente = new Map([ [ 'nombre', 'paciente'], ['cuarto', 'no-definido']]); //dos arreglos dentro de un arreglo

paciente.set('dr', 'Dr Asignado') //se agrega un valor y llave 
paciente.set('nombre', 'Antonio') // si la llave se llama igual que otra y se agrega un valor, este se reasigna

console.log(paciente);

paciente.forEach( (datos, index) => {
    console.log(index); //en este caso el segundo calor reporta la llave
})
