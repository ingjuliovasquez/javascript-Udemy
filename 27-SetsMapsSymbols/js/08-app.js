 //NUEVOS ITERADORES 
const ciudades = ['Londres', 'New York', 'Madrid', 'Paris'];
const ordenes = new Set([123, 231, 131, 102]);
const datos = new Map();

//llave es el primero, valor es el segundo 
datos.set('nombre', 'Madi')
datos.set('profesion', 'Desarrollador Web');

// iterar por DEFAULT 
for(let ciudad of ciudades ) { //trae los valores
    console.log(ciudad)
}
for(let orden of ordenes ) {//trae los valores
    console.log(orden)
}
for(let dato of datos ) { //se trae llave y valor 
    console.log(dato)
}
//KEYS iterador imprime las LLAVES
/*for( let keys of ciudades.keys()) {
    console.log(keys) //imprime las posiciones en las que se enuentra el arreglo
}
for( let keys of ordenes.keys()) { //como un set NO tienen llaves, toma los valores 
    console.log(keys) 
}
for( let keys of datos.keys()) { //imprikme las llaves en este ejemplo son nombre y profesion
    console.log(keys) 
}*/


//value iterador , es decir imprime los VALORES 
/*for( let value of ciudades.values()) { //imprime el nombre de las ciudades 
    console.log(value)
}
for( let value of ordenes.values()) { //imprime los numeros 
    console.log(value)
}
for( let value of datos.values()) { //imprime madi y desarrollador web 
    console.log(value)
}*/


// entries iterador imprime LLAVE y VALOR en un solo arreglo
/*for( let entry of ciudades.entries()) { //devuelve un nuevo objeto que contiene llave y valor
    console.log(entry) 
}
for( let entry of ordenes.entries()) {  //le crea una llave con los mismos datos de valolr
    console.log(entry) 
}
for( let entry of datos.entries()) { //imprime la llave y el valor   
    console.log(entry) 
}
*/