// WEAKSET es decir un set debil

const weakset = new WeakSet();

const cliente = {
    nombre: 'Juan',
    saldo: 100
}
weakset.add(cliente); //solo aceptan objetos, no aceptan ningun otro valor 


/*const nombre = 20; 
console.log(nombre) //indicara un error porque no acpta ningun metodo*/

//*console.log(weakset.has('cliente')); //nos indica si existe o no  */

//weakset.delete(cliente) //elimina el objeto del weak}Set
console.log(cliente.size)  //no podemos conocer la extencion de weakSet


//LA DIFERECIA ENTRE UN SET Y UN WEAKSET  

// SET puede almacenar cualquier tipo de valor 
//WEAKSET solo almacena objetos no son iterables por lo tanto no podemos usar forEach


console.log(weakset)