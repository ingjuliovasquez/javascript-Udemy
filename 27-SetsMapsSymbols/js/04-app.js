//WEAEMAP mantiene una serie de datos privados utiliza llave y producto, no mse pueden iterar por lo tanto no se puede ocupar forEach

const producto = {
    idProducto: 10
}

const weakmap = new WeakMap();

weakmap.set(producto, 'Monitor'); //solo nos permite conocer el valor, la llave esta oculta

console.log( weakmap.has(producto)); //nos dice si existe el objeto
console.log( weakmap.get(producto)); //nos permite obetener un valor
console.log( weakmap.delete(producto)); //nos permite eliminar
//console.log(weakmap.size); //NO podemos conocer la extencion 

console.log(weakmap)