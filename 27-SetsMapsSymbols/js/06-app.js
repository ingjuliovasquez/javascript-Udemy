//ITERADORES

function crearIterador(carrito) {

    let i = 0; //inicia contando desde el 0

    return{
        siguiente: () => {

            const fin = ( i >= carrito.length ); //si el indice es mayor o igual al carrito entonces hemos llegado al final que es la posicion 3
            const valor = !fin ? carrito[i++] : undefined; //si no hemos llegado al final es decir a la posicion  final al poner i++ significa que vamos sumando 

            return{
                fin, 
                valor
            }

        }
    }
}
const carrito = ['Producto 1', 'Producto 2', 'Producto 3'];

//Utilizar el iterador 
const recorrerCarrito = crearIterador(carrito);

console.log(recorrerCarrito.siguiente()); //recorre la funcion de siguiente 
console.log(recorrerCarrito.siguiente());//sera false
console.log(recorrerCarrito.siguiente());//sera false
console.log(recorrerCarrito.siguiente()); //cuando yo no alla mas valores que recorrer sera true