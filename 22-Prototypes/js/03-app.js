function Cliente(nombre, saldo) {
    this.nombre = nombre;
    this.saldo = saldo;
} 

Cliente.prototype.tipoCliente = function() {
    //se define un nuevo proto llamado tipoCliente exclusiva del objeto cliente, este no se pasara a los siguientes objetos 

    let tipo; 

    if(this.saldo > 10000) {
        tipo = 'Gold';
    } else if(this.saldo > 5000) {
        tipo = 'Platinum';
    } else {
        tipo = 'Normal';
    }
    return tipo;
}

Cliente.prototype.nombreClienteSaldo = function() { //proto designado unicamente al cliente
    return `Nombre: ${this.nombre}, Saldo: ${this.saldo}, Tipo Cliente: ${this.tipoCliente()}`
}

Cliente.prototype.retiraSaldo = function(retira) { //proto designado unicamente al cliente
    this.saldo -= retira
}


//INSTANCIARLO
const pedro = new Cliente('Pedro', 6000);
console.log( pedro.tipoCliente()); //nos dice el tipo de cliente
console.log(pedro.nombreClienteSaldo()); //
pedro.retiraSaldo(1000);
console.log( pedro.nombreClienteSaldo());

console.log(pedro);


