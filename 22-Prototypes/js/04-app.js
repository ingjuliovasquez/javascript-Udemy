function Cliente(nombre, saldo) {
    this.nombre = nombre;
    this.saldo = saldo;
} 

Cliente.prototype.tipoCliente = function() {
    //se define un nuevo proto llamado tipoCliente exclusiva del objeto cliente, este no se pasara a los siguientes objetos 

    let tipo; 

    if(this.saldo > 10000) {
        tipo = 'Gold';
    } else if(this.saldo > 5000) {
        tipo = 'Platinum';
    } else {
        tipo = 'Normal';
    }
    return tipo;
}

Cliente.prototype.nombreClienteSaldo = function() { //proto designado unicamente al cliente
    return `Nombre: ${this.nombre}, Saldo: ${this.saldo}, Tipo Cliente: ${this.tipoCliente()}`
}

Cliente.prototype.retiraSaldo = function(retira) { //proto designado unicamente al cliente
    this.saldo -= retira
}

function Persona(nombre, saldo, telefono) {
    Cliente.call(this, nombre, saldo); //hereda los prototypes del la funcion cliente 
    this.telefono = telefono; //y se le anexa la nueva propiedad 
}


//SE DEBE CREAR ANTES DE INSTANCIARLOS PARA QUE LOS  VALORES PUEDAN SER HEREDABLES 
Persona.prototype = Object.create( Cliente.prototype); //hereda las funciones
Persona.prototype.constructor = Cliente; //hereda el constructor 

//INSTANCIARLO
const juan = new Persona('juan', 5000, 14551455);
console.log(juan)
console.log(juan.nombreClienteSaldo());

Persona.prototype.mostrarTelefono = function() { //es un proto exclusivo de persona
    return `El telefono de esta persona ${this.telefono}`;
}
console.log(juan.mostrarTelefono())