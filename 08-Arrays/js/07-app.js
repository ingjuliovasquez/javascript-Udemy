const carrito = []; //Al entrar a una pagina se empieza con el carrito vacio 

//Definir un producto
const producto = {  
    nombre: "Monitor 32 pulgadas",
    precio: 400
}

const producto2 = {
    nombre: "Celular",
    precio: 800
}
const producto4 = {
    nombre: "Celular 2",
    precio: 1000
}

//.push agrega al final de un arreglo 

carrito.push(producto2); //Se agrego al carrito un producto
carrito.push(producto);//Se agrego al carrito dos producto
carrito.push(producto4);

const producto3 = {
    nombre: "Teclado",
    precio: 50
}

carrito.unshift(producto3); //Hace que aparezca en primer lugar de la lista de productos agregados al carrito
console.table(carrito) //Enlosta los productos 


/*/Eliminar ultimo elemento de un arreglo..
carrito.pop();
console.table(carrito);

//Eliminar del inicio del arreglo 
carrito.shift();
console.table(carrito);*/

carrito.splice(3, 1); //elimina el producto de la tercera posicion 
carrito.splice(0, 1) //elimina el producto de la posicion 0
console.table(carrito); 
