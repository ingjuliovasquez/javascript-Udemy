const producto = {
    nombre: "Monitor de 20 pulgadas",
    precio: 300,
    disponible: true,
}

/*const nombre = producto.nombre;
console.log(nombre);*/

//Destructuring o destructurando  permite desempacar valores de arreglos o propiedades de objetos en distintas variables.
const { disponible} = producto;

console.log(disponible);

//Destructuring con arreglos 
const numeros = [10,20,30,40,50];
const [ , , tercero] = numeros; //destructuring solo el tercer numero es 30
const [ primero, ...cuarto] = numeros;

console.log(tercero);
console.log(cuarto); //destructuring los ultimos 4 numero es decir 20,30,40,50
