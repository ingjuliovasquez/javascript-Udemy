const numeros = [10, 20, 30]; //los arreglos van dentro decorchetes

const meses = new Array('Enero, Febrero, Marzo'); //usa parentesis porque es um nuevo constructor


console.log(numeros);
console.log(meses);

// ejemplo de arreglo: En facebook todas las personas que le dan like a una foto


//Un arreglo que contiene datos de todo tipo
const deTodo = ["Hola", 10, true, "si", null, {nombre: 'Maadai', trabajo: 'Programador'}, [1, 2, 3]]; //se  puede hacerun arreglo dentro de un arreglo

console.log(deTodo);