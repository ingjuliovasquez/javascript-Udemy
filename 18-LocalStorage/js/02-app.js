const nombre = localStorage.getItem('nombre'); //obtiene un valor
console.log(nombre);

const productoJSON = localStorage.getItem('producto'); 
console.log(JSON.parse( productoJSON)); //convierte un string en un objeto o un arreglo

const meses = localStorage.getItem('meses');
const mesesArray = JSON.parse(meses);
console.log(mesesArray)