const container = document.querySelector('.container');
const resultado = document.querySelector('#resultado');
const formulario = document.querySelector('#formulario');

window.addEventListener('load', () => { //lead es parecido al DOM
    formulario.addEventListener('submit', buscarClima);
})

function buscarClima(e) {
    e.preventDafault();

    //Validar
    const ciudad = document.querySelector('#ciudad').value //seleccionamos el value del id de ciudad 
    const pais = document.querySelector('#pais').value 

    console.log(ciudad);
    console.log(pais);

    if(ciudad === '' || pais === '') { //si ambos campos estan vacios maracara un mensaje de error 
        //hubo un error
        mostrarError('Ambos campos son obligatorios');

        return;//si ambos campos estan vacios no se seguira ejecutando el demas codigo

    }

    //Consultar la API
    consultarAPI(ciudad, pais);
}

function mostrarError(mensaje) {
    const alerta = document.querySelector('.bg-red-100')
    
    if(alerta) {

        //crear una alerta 
        const alerta = document.createElement('div');

        alerta.classList.add('bg-red-100', 'border-red-400', 'text-red-400', 'text-red-700', 'px-4', 'py-3', 'rounded', 'max-w-nd', 'mx-auto', 'mt-6', 'text-center');

        alerta.innerHTML = `
        <strong class="font-bold">Error!</strong>
        <span class"block">$</span>
        `;

        container.appendChild(alerta); //agregamos la alerta al final

        //eliminar la alerta despues de 5 segundos 
        setTimeout(() => {
            alerta.remove();
        }, 5000);
    }
}

function consultarAPI(ciudad, pais) {
    const appId = 'b0f3f434aa3af051f8ce2295eab2a583'; //solo es para fines comenrciales de la misma pagina que ofrece la API

    const url = `https://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${appId}`;

    Spinner(); //muestra un spinner de carga antes de consultar el clima 

    fetch(url) 
        .then ( respuesta => respuesta.json()) 
        .then ( datos => {

            limpiarHTML(); //limpiar el HTML previo de un error 

            if(datos.cod === "404") { //si el codigo da algun error aparecera una alerta 
                mostrarError('Ciudad no encontrada')
            }

            //imprimir la respuesta en el HTML
            mostrarClima(datos);
        })
}

function mostrarClima(datos) {
    const {name, main: {temp, temp_max, temp_min}} = datos;

    const centrigados = kelvinACentigrados(temp);//formula que convierte a grados centrigados 
    const max = kelvinACentigrados(temp_max);
    const min = kelvinACentigrados(temp_min);

    const nombreCiudad = document.createElement('p');
    nombreCiudad.textContent = `Clima en ${name}`
    nombreCiudad.classList.add('font-bold', 'text-2xl')

    const actual = document.createElement('p');
    actual.innerHTML = `${centrigados} &#8451`; 
    actual.classList.add('font-bolt', 'text-6xl');

    const tempMaxima = document.createElement('p');
    tempMaxima.innerHTML = `Max: ${max} &#8451`;
    tempMaxima.classList.add('text-xl');

    const tempMinima = document.createElement('p');
    tempMinima.innerHTML = `Min: ${min} &#8451`;
    tempMinima.classList.add('text-xl');

    const resultadoDiv = document.createElement('div');
    resultadoDiv.classList.add('text-center', 'text-write');

    resultadoDiv.appendChild(nombreCiudad);
    resultadoDiv.appendChild(actual)
    resultadoDiv.appendChild(tempMaxima);
    resultadoDiv.appendChild(tempMinima);

    resultado.appendChild(resultadoDiv);
}

const kelvinACentigrados = grados => parseInt(grados -273.15) //Un function puede simplicarse como un arrawfuntion porque solo lleva una linea de codigo, 

function limpiarHTML() {
    while(resultado.firstChild) {
        resultado.removeChild(resultado.firstChild);
    }
}

function Spinner( ) {

    limpiarHTML();
    const divSpinner = document.createElement('div');
    divSpinner.classList.add('sk-fading-circle');
    divSpinner.innerHTML = `
        <div class="sk-circle1 sk-circle"></div>
        <div class="sk-circle2 sk-circle"></div>
        <div class="sk-circle3 sk-circle"></div>
        <div class="sk-circle4 sk-circle"></div>
        <div class="sk-circle5 sk-circle"></div>
        <div class="sk-circle6 sk-circle"></div>
        <div class="sk-circle7 sk-circle"></div>
        <div class="sk-circle8 sk-circle"></div>
        <div class="sk-circle9 sk-circle"></div>
        <div class="sk-circle10 sk-circle"></div>
        <div class="sk-circle11 sk-circle"></div>
        <div class="sk-circle12 sk-circle"></div>
    `;

    resultado.appendChild(divSpinner);
}